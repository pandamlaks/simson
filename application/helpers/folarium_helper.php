<?php

function no_order($value) {

    $jml = strlen($value);
    if ($jml == 1)
        $no = "000000" . $value;
    if ($jml == 2)
        $no = "00000" . $value;
    if ($jml == 3)
        $no = "0000" . $value;
    if ($jml == 4)
        $no = "000" . $value;
    if ($jml == 5)
        $no = "00" . $value;
    if ($jml == 6)
        $no = "0" . $value;
    if ($jml == 7)
        $no = $value;
    if ($jml == 0)
        $no = "0000001";

    return $no;
}

function table_no($value) {

    $jml = strlen($value);
    if ($jml == 1)
        $no = "0" . $value;
    if ($jml == 2)
        $no = $value;
    if ($jml == 0)
        $no = "01";

    return "No. $no";
}

function table_no_cetak($value) {

    $jml = strlen($value);
    if ($jml == 1)
        $no = "0" . $value;
    if ($jml == 2)
        $no = $value;
    if ($jml == 0)
        $no = "01";

    return "$no";
}

function fin_code($value) {
    $jml = strlen($value);
    if ($jml == 1)
        $no = "000000" . $value;
    if ($jml == 2)
        $no = "00000" . $value;
    if ($jml == 3)
        $no = "0000" . $value;
    if ($jml == 4)
        $no = "000" . $value;
    if ($jml == 5)
        $no = "00" . $value;
    if ($jml == 6)
        $no = "0" . $value;
    if ($jml == 7)
        $no = $value;
    if ($jml == 0)
        $no = "0000001";

    return "NUP$no";
}

function fin_status($x) {
    if ($x != null) {
        if ($x == 1) {
            return '<div style="background : #d9534f; border-color: #d43f3a; color: white; padding: 4px; font-size: 12px; margin: 0px"><i class="fa fa-times mg-r-sm mg-l-md"></i> Cancel</div>';
        } else if ($x == 2) {
            return '<div style="background : #f0ad4e; border-color: #eea236;color: white; padding: 4px; font-size: 12px; margin: 0px"><i class="fa fa-minus-circle mg-r-sm mg-l-md"></i> Updated</div>';
        } else {
            return '<div style="background : #5cb85c; border-color: #4cae4c;color: white; padding: 4px; font-size: 12px; margin: 0px"><i class="fa fa-check mg-r-sm mg-l-md"></i> Verified</div>';
        }
    }
}

function status_table($x) {
    if ($x != null) {
        if ($x == 1) {
            return '<div style="background : #5cb85c; border-color: #4cae4c;color: white; padding: 4px; font-size: 12px; margin: 0px"><i class="fa fa-check mg-r-sm mg-l-md"></i>Digunakan</div>';
        } else {
            return '<div style="background : #d9534f; border-color: #d43f3a; color: white; padding: 4px; font-size: 12px; margin: 0px"><i class="fa fa-minus-circle mg-r-sm mg-l-md"></i>Kosong</div>';
        }
    } else {
        return '<div style="background : #5cb85c; border-color: #4cae4c;color: white; padding: 4px; font-size: 12px; margin: 0px"><i class="fa fa-check mg-r-sm mg-l-md"></i>Digunakan</div>';
    }
}

//
//function status_bayar($x) {
//    if ($x != null) {
//        if ($x == 0) {
//            return 'Belum Terbayar';
//        } else if ($x == 1) {
//            return 'Terbayar';
//        } else if ($x == 2) {
//            return 'Batal';
//        }
//    }
//}

function status_antar($x) {
    if ($x != null) {
        if ($x == 0) {
            return '<div style="background : #f0ad4e; border-color: #eea236;color: white; padding: 4px; font-size: 12px; margin: 0px"><i class="fa fa-minus-circle mg-r-sm mg-l-md"></i>Belum Diantar</div>';
        } else if ($x == 1) {
            return '<div style="background : #5cb85c; border-color: #4cae4c;color: white; padding: 4px; font-size: 12px; margin: 0px"><i class="fa fa-check mg-r-sm"></i>Sudah Diantar</div>';
        } else if ($x == 2) {
            return '<div style="background : #d9534f; border-color: #d43f3a; color: white; padding: 4px; font-size: 12px; margin: 0px"><i class="fa fa-minus-circle mg-r-sm"></i>Batal</div>';
        }
    }
}

function status_menu($x) {
    if ($x != null) {
        if ($x == 0) {
            return '<div style="background : #d9534f; border-color: #d43f3a; color: white; padding: 4px; font-size: 12px; margin: 0px"><i class="fa fa-minus-circle mg-r-sm"></i>Kosong</div>';
        } else if ($x == 1) {
            return '<div style="background : #5cb85c; border-color: #4cae4c;color: white; padding: 4px; font-size: 12px; margin: 0px"><i class="fa fa-check mg-r-sm"></i>Tersedia</div>';
        }
    }
}

function status_bayar($x) {
    if ($x != null) {
        if ($x == 0) {
            return '<div style="background : #d9534f; border-color: #d43f3a; color: white; padding: 4px; font-size: 12px; margin: 0px"><i class="fa fa-minus-circle mg-r-sm"></i>Belum dibayar</div>';
        } else if ($x == 1) {
            return '<div style="background : #5cb85c; border-color: #4cae4c;color: white; padding: 4px; font-size: 12px; margin: 0px"><i class="fa fa-check mg-r-sm"></i>Sudah dibayar</div>';
        } else if ($x == 3) {
            return '<div style="background : #5cb85c; border-color: #4cae4c;color: white; padding: 4px; font-size: 12px; margin: 0px"><i class="fa fa-check mg-r-sm"></i>Batal</div>';
        }
    }
}

function rupiah($jml, $dcm = null) {
    $nominal = isset($dcm) ? $dcm : 0;
    $int = empty($jml) ? 0 : number_format($jml, $nominal, ',', '.');
    return $int;
}

function timeToReal($time) {
    $y = date("H:i", strtotime($time));
    return $y;
}

function rounding($jml) {
    $int = number_format($jml, 2, ',', '.');
    $angka = explode('.', $jml);
    if ($angka[1] <= 25) {
        return $angka[0];
    } else if ($angka[1] <= 75) {
        return intval($angka[0]) + 0.5;
    } else {
        return intval($angka[0]) + 1;
    }
}

function date2mysql($dates, $time = null) {
    // $date = explode('-', $dates);
    // $show_time = isset($time) ? "00:00:00" : null;
    // return "$date[2]-$date[1]-$date[0] $show_time";
    return date('Y-m-d', strtotime($dates));
}

function indo_date($date, $length = null, $show_hour = null) {
    $datetime = explode(' ', $date);

    if (empty($datetime[1])) {
        $get_hour = null;
    } else {
        if (isset($show_hour)) {
            $hour = explode(":", $datetime[1]);
            $get_hour = "/ $hour[0]:$hour[1]";
        } else {
            $get_hour = null;
        }
    }
    $tgl = explode("-", $datetime[0]);
    if ($tgl[1] == '01')
        $mo = empty($length) ? "Januari" : "Jan";
    if ($tgl[1] == '02')
        $mo = empty($length) ? "Februari" : "Feb";
    if ($tgl[1] == '03')
        $mo = empty($length) ? "Maret" : "Mar";
    if ($tgl[1] == '04')
        $mo = empty($length) ? "April" : "Apr";
    if ($tgl[1] == '05')
        $mo = "Mei";
    if ($tgl[1] == '06')
        $mo = empty($length) ? "Juni" : "Jun";
    if ($tgl[1] == '07')
        $mo = empty($length) ? "Juli" : "Jul";
    if ($tgl[1] == '08')
        $mo = empty($length) ? "Agustus" : "Agust";
    if ($tgl[1] == '09')
        $mo = empty($length) ? "September" : "Sept";
    if ($tgl[1] == '10')
        $mo = empty($length) ? "Oktober" : "Okt";
    if ($tgl[1] == '11')
        $mo = empty($length) ? "November" : "Nov";
    if ($tgl[1] == '12')
        $mo = empty($length) ? "Desember" : "Des";
    $convert = "$tgl[2] $mo $tgl[0] $get_hour";

    return $convert;
}

function calculate_age($tgl) {
    $tanggal = explode("/", $tgl);
    $tahun = $tanggal[2];
    $bulan = $tanggal[1];
    $hari = $tanggal[0];

    $day = date('d');
    $month = date('m');
    $year = date('Y');

    $tahun = $year - $tahun;
    $bulan = $month - $bulan;
    $hari = $day - $hari;

    $jumlahHari = 0;
    $bulanTemp = ($month == 1) ? 12 : $month - 1;
    if ($bulanTemp == 1 || $bulanTemp == 3 || $bulanTemp == 5 || $bulanTemp == 7 || $bulanTemp == 8 || $bulanTemp == 10 || $bulanTemp == 12) {
        $jumlahHari = 31;
    } else if ($bulanTemp == 2) {
        if ($tahun % 4 == 0)
            $jumlahHari = 29;
        else
            $jumlahHari = 28;
    }else {
        $jumlahHari = 30;
    }

    if ($hari < 0) {
        $hari+=$jumlahHari;
        $bulan--;
    }
    if ($bulan < 0 || ($bulan == 0 && $tahun != 0)) {
        $bulan+=12;
        $tahun--;
    }
    if ($bulan == 12) {
        $bulan = 0;
        $tahun += 1;
    }
    if ($tahun == '0') {
        $tahunz = '';
    } else {
        $tahunz = $tahun . " Tahun ";
    }
    return $tahunz . $bulan . " Bulan " . $hari . " Hari";
}

function get_hour($jam) {
    $var = explode(" ", $jam);
    return $var[1];
}

function show_array($array) {
    echo "<pre>";
    print_r($array);
    echo "</pre>";
}

function selisih_hari($startDate, $endDate) {
    $tgl1 = $startDate;  // 1 Oktober 2009
    $tgl2 = $endDate;  // 10 Oktober 2009
    // memecah tanggal untuk mendapatkan bagian tanggal, bulan dan tahun
    // dari tanggal pertama
//    echo "$tgl1 $tgl2";
    $pecah1 = explode("-", $tgl1);

    $date1 = $pecah1[2];
    $month1 = $pecah1[1];
    $year1 = $pecah1[0];

    // memecah tanggal untuk mendapatkan bagian tanggal, bulan dan tahun
    // dari tanggal kedua

    $pecah2 = explode("-", $tgl2);
    $date2 = $pecah2[2];
    $month2 = $pecah2[1];
    $year2 = $pecah2[0];

    // menghitung JDN dari masing-masing tanggal

    $jd1 = GregorianToJD($month1, $date1, $year1);
    $jd2 = GregorianToJD($month2, $date2, $year2);

    // hitung selisih hari kedua tanggal

    $selisih = $jd2 - $jd1;
    return $selisih;
}

function romawi($num) {
    switch ($num) {
        case 1:
            $romawi = 'I';
            break;
        case 2:
            $romawi = 'II';
            break;
        case 3:
            $romawi = 'III';
            break;
        case 4:
            $romawi = 'IV';
            break;
        case 5:
            $romawi = 'V';
            break;
        case 6:
            $romawi = 'VI';
            break;
        default:
            $romawi = '';
            break;
    }
    return $romawi;
}
