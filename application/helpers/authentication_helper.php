<?php

if (!function_exists('is_logged_in')) {

    function is_logged_in() {
        $CI = & get_instance();
        $is_logged_in = $CI->session->userdata(md5('for_order'));
        if (!isset($is_logged_in) || $is_logged_in != true) :
            redirect('');
        endif;
    }

    function is_filtered($level) {
        $CI = & get_instance();
        $is_logged_in = $CI->session->userdata(md5('for_order'));
        if (md5($is_logged_in['level_id']) != $level) :
            redirect("home");
        endif;
    }

}