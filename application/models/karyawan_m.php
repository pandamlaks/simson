<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan_m extends CI_Model {

    public function show() {
        $this->db->order_by("id_user", "DESC");
        return $this->db->get('tm_karyawan')->result();
    }
    
     public function karyawan() {
        //$query = '';
        $query = "SELECT * "
                . "FROM tm_karyawan INNER JOIN tm_user ON (tm_user.id_user=tm_karyawan.id_user) ORDER BY (nama) ";
//                . "INNER JOIN (SELECT id_mahasiswa FROM tt_kerja_status GROUP BY id_kerja) AS tm  ON (tks.id_mahasiswa=tm_mahasiswa.id_mahasiswa) "
//                . "INNER JOIN tr_status ON (tks.id_status=tr_status.id_status)";
        $res = $this->db->query($query);
        return $res->result();
    }

}
