<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_m extends CI_Model {

    function insert() {
        $this->db->set('id_role', $this->input->post('role'));
        $this->db->set('nama', $this->input->post('nama'));
        $this->db->set('nomor', $this->input->post('nim'));
        $this->db->set('jurusan', $this->input->post('jurusan'));
        $this->db->set('id_konsentrasi', $this->input->post('konsentrasi'));
        $this->db->set('passwd', $this->input->post('password'));
        $this->db->set('email', $this->input->post('email'));
        $this->db->set('kontak', $this->input->post('kontak'));
        return $this->db->insert('tm_user');
    }

    function profil($id = null) {
        $query = "SELECT * FROM tm_user "
                . "LEFT JOIN tr_konsentrasi ON (tm_user.id_konsentrasi=tr_konsentrasi.id_konsentrasi) "
                . "WHERE md5(tm_user.id_user)='$id'";
        $res = $this->db->query($query);
        return $res->row();
    }

}
