<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Konsentrasi extends CI_Model {

    public function show($konsen = null) {
        $konsen_convert = !empty($konsen) ? str_replace("-", " ", $konsen) : null;
        $where = !empty($konsen) ? "where jurusan = '$konsen_convert'" : null;
        $query = "SELECT * FROM tr_konsentrasi $where";
        $res = $this->db->query($query);
        return $res->result();
    }

    public function role() {
        //$query = '';
        $query = "SELECT * FROM tr_role where id_role !=6";
        $res = $this->db->query($query);
        return $res->result();
    }

}
