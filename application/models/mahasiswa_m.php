<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa_m extends CI_Model {

    public function show() {
        $this->db->order_by("id_user", "DESC");
        return $this->db->get('tm_user')->result();
    }

    public function status() {
        //$query = '';
        $query = "SELECT tm_user.nama AS mhs, tr_status.nama AS status "
                . "FROM tm_kerja INNER JOIN tm_user ON (tm_user.id_user=tm_kerja.id_mahasiswa) "
                . "INNER JOIN (SELECT id_kerja, max(id_status) AS id_status FROM tt_kerja_status GROUP BY id_kerja) AS tks  ON (tks.id_kerja=tm_kerja.id_kerja) "
                . "INNER JOIN tr_status ON (tks.id_status=tr_status.id_status)";
        $res = $this->db->query($query);
        return $res->result();
    }

    public function mahasiswa() {
        //$query = '';
        $query = "SELECT * "
                . "FROM tm_mahasiswa INNER JOIN tm_user ON (tm_user.id_user=tm_mahasiswa.id_user) "
                . "LEFT JOIN tm_kerja ON (tm_kerja.id_mahasiswa=tm_mahasiswa.id_mahasiswa) ORDER BY (nomor) ";
//                . "INNER JOIN (SELECT id_mahasiswa FROM tt_kerja_status GROUP BY id_kerja) AS tm  ON (tks.id_mahasiswa=tm_mahasiswa.id_mahasiswa) "
//                . "INNER JOIN tr_status ON (tks.id_status=tr_status.id_status)";
        $res = $this->db->query($query);
        return $res->result();
    }

    public function pengawasan($status = null) {
        $where = '(select datediff(now(),tgl_eksekusi) from tt_kerja_status where id_kerja = tm_kerja.id_kerja and id_status = 5 ) > 180';
        $where2 = '(select max(id_status) from tt_kerja_status where id_kerja = tm_kerja.id_kerja) < 6';
        if($status == 3){
            $where = '(select datediff(now(),tgl_eksekusi) from tt_kerja_status where id_kerja = tm_kerja.id_kerja and id_kerja_status = (select max(id_kerja_status) from tt_kerja_status where id_kerja = tm_kerja.id_kerja and id_status = 6) and id_status = 6) > 30';
            $where2 = '(select max(id_status) from tt_kerja_status where id_kerja = tm_kerja.id_kerja) < 7';
        }elseif($status == 4){
            $where = '(select datediff(now(),tgl_eksekusi) from tt_kerja_status where id_kerja = tm_kerja.id_kerja and id_status = 7) > 30';
            $where2 = '(select max(id_status) from tt_kerja_status where id_kerja = tm_kerja.id_kerja) < 8';
        }elseif($status == 5){
            $where = '(select datediff(now(),tgl_eksekusi) from tt_kerja_status where id_kerja = tm_kerja.id_kerja and id_status = 8) > 30';
            $where2 = '(select max(id_status) from tt_kerja_status where id_kerja = tm_kerja.id_kerja) < 9';
        }
        $query = $query = "SELECT *,dsn.nama as dosen, tm_user.nama as mhs, tm_user.nomor as nim, tm_user.kontak as kontak, "
                . "(select tgl_eksekusi from tt_kerja_status where id_kerja = tm_kerja.id_kerja and id_status = 5) as mulai, (select tgl_eksekusi from tt_kerja_status where id_kerja = tm_kerja.id_kerja and id_status = 1) as daftar, "
                . "(select tgl_eksekusi from tt_kerja_status where id_kerja = tm_kerja.id_kerja and id_status = 6 and id_kerja_status = (select max(id_kerja_status) from tt_kerja_status where id_kerja = tm_kerja.id_kerja and id_status = 6)) as bimbingan "
                . "FROM ( SELECT tm_kerja.id_kerja, MAX(id_status) AS status, tgl_eksekusi FROM tm_kerja "
                . "INNER JOIN tt_kerja_status ON (tm_kerja.id_kerja=tt_kerja_status.id_kerja) GROUP BY tm_kerja.id_kerja ) "
                . "AS tmax INNER JOIN tm_kerja ON (tm_kerja.id_kerja=tmax.id_kerja) "
                . "INNER JOIN tm_mahasiswa ON (tm_mahasiswa.id_mahasiswa=tm_kerja.id_mahasiswa) "
                . "INNER JOIN tm_user ON (tm_mahasiswa.id_user=tm_user.id_user) "
                . "LEFT JOIN tm_karyawan ON (tm_karyawan.id_karyawan=tm_kerja.id_karyawan) "
                . "LEFT JOIN tm_user dsn ON (tm_karyawan.id_user=dsn.id_user)"
                . "where $where and $where2";
                
        $res = $this->db->query($query);
        return $res->result();
    }

    

}
