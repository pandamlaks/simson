<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Content_m extends CI_Model {

    public function show() {
        $this->db->order_by("c.id_content", "DESC");
        $this->db->join("tm_karyawan k", "k.id_karyawan = c.id_karyawan");
        $this->db->join("tm_user u", "u.id_user = k.id_user");
        return $this->db->get('content c')->result();
    }

    public function detail($id) {
        $this->db->join("tm_karyawan k", "k.id_karyawan = c.id_karyawan");
        $this->db->join("tm_user u", "u.id_user = k.id_user");
        $this->db->where("c.id_content = $id");
        return $this->db->get('content c')->row();
    }

    public function document() {
        $this->db->order_by("c.id_document", "DESC");
        $this->db->limit(5);
        $this->db->join("tm_karyawan k", "k.id_karyawan = c.id_karyawan");
        $this->db->join("tm_user u", "u.id_user = k.id_user");
        return $this->db->get('document c')->result();
    }
        
}
