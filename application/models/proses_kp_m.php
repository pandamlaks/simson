<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Proses_kp_m extends CI_Model {
    public function kp($id = null) {        
       
       $query = $query = "SELECT *,dsn.nama as dosen, tm_user.nama as mhs, tm_user.nomor as nim, tm_user.kontak as kontak, "
                . "(select tgl_eksekusi from tt_kerja_status where id_kerja = tm_kerja.id_kerja and id_status = 5) as mulai, (select tgl_eksekusi from tt_kerja_status where id_kerja = tm_kerja.id_kerja and id_status = 1) as daftar, "
                . "(select tgl_eksekusi from tt_kerja_status where id_kerja = tm_kerja.id_kerja and id_status = 2) as permohonan, (select tgl_eksekusi from tt_kerja_status where id_kerja = tm_kerja.id_kerja and id_status = 4) as diterima, "
                . "(select tgl_eksekusi from tt_kerja_status where id_kerja = tm_kerja.id_kerja and id_status = 6 and id_kerja_status = (select max(id_kerja_status) from tt_kerja_status where id_kerja = tm_kerja.id_kerja and id_status = 6)) as bimbingan "
                . "FROM ( SELECT tm_kerja.id_kerja, MAX(id_status) AS status, tgl_eksekusi FROM tm_kerja "
                . "INNER JOIN tt_kerja_status ON (tm_kerja.id_kerja=tt_kerja_status.id_kerja) GROUP BY tm_kerja.id_kerja ) "
                . "AS tmax INNER JOIN tm_kerja ON (tm_kerja.id_kerja=tmax.id_kerja) "
                . "INNER JOIN tm_mahasiswa ON (tm_mahasiswa.id_mahasiswa=tm_kerja.id_mahasiswa) "
                . "INNER JOIN tm_user ON (tm_mahasiswa.id_user=tm_user.id_user) "
                . "LEFT JOIN tm_karyawan ON (tm_karyawan.id_karyawan=tm_kerja.id_karyawan) "
                . "LEFT JOIN tm_user dsn ON (tm_karyawan.id_user=dsn.id_user) "
                . "WHERE md5(tm_user.id_user)='$id'";
        $res = $this->db->query($query);
        return $res->result();
    }
}