<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Selesai_m extends CI_Model {

    public function rekap() {

        $query = "SELECT * "
                . "FROM ( SELECT tm_kerja.id_kerja, MAX(id_status) AS status, tgl_eksekusi FROM tm_kerja "
                . "INNER JOIN tt_kerja_status ON (tm_kerja.id_kerja=tt_kerja_status.id_kerja) GROUP BY tm_kerja.id_kerja ) "
                . "AS tmax INNER JOIN tm_kerja ON (tm_kerja.id_kerja=tmax.id_kerja) "
                . "INNER JOIN tm_mahasiswa ON (tm_mahasiswa.id_mahasiswa=tm_kerja.id_mahasiswa) "
                . "INNER JOIN tm_user ON (tm_mahasiswa.id_user=tm_user.id_user) "
                . "INNER JOIN ( SELECT id_kerja, tgl_eksekusi AS tgl_daftar FROM tt_kerja_status "
                . "WHERE id_status=1) "
                . "AS tdaf ON (tdaf.id_kerja=tm_kerja.id_kerja) "
                . "WHERE status=8";

        $res = $this->db->query($query);
        return $res->result();
    }

}
