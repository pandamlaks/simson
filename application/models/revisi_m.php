<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Revisi_m extends CI_Model {

    public function revisi($id = null) {
        $query = "SELECT *, (select max(id_status) from tt_kerja_status where id_kerja = tm_kerja.id_kerja) as last FROM tm_kerja JOIN tm_karyawan k ON k.id_karyawan = tm_kerja.id_karyawan"
                . " JOIN tt_kerja_status ttks ON ttks.id_kerja = tm_kerja.id_kerja "
                . " JOIN tm_mahasiswa tmm ON tmm.id_mahasiswa = tm_kerja.id_mahasiswa"
                ." LEFT JOIN tt_ujian ttj on ttj.id_kerja = tm_kerja.id_kerja"
                . " WHERE ttks.id_status=7 AND md5(tmm.id_user)='$id'";
        $res = $this->db->query($query);
        return $res->row();
    }

    public function dosen($id_kerja) {
        $query = "SELECT tmu.* "
                . "FROM tm_kerja INNER JOIN tm_karyawan ON (tm_karyawan.id_karyawan=tm_kerja.id_karyawan)"
                . "INNER JOIN (SELECT * FROM tm_user) AS tmu ON (tmu.id_user=tm_karyawan.id_user) where tm_kerja.id_kerja ='$id_kerja'";
        $res = $this->db->query($query);
        return $res->result();
    }

}
