<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Crud_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function insert_data($table, $data) {
        $insertSQL = $this->db->insert($table, $data);
        return $insertSQL;
    }

    public function update_data($table, $content, $selection) {
        $updateSQL = $this->db->where($selection, $content[$selection]);
        $updateSQL = $this->db->update($table, $content);
        // return $updateSQL;
    }
public function read_data($table, $content) {
        $updateSQL = $this->db->where($content);
        $updateSQL = $this->db->get($table);
        // return $updateSQL;
    }
    public function delete_data($table, $selection1, $id1, $selection2 = null, $id2 = null) {
        $deleteSQL = $this->db->where($selection1, $id1);
        if ($selection2 != null && $id2 != null) {
            $deleteSQL = $this->db->where($selection2, $id2);
        }
        $deleteSQL = $this->db->delete($table);
        return $deleteSQL;
    }

    public function json_autocomplete($search, $table, $column) {
        $sql = "SELECT * FROM $table WHERE $column LIKE '%$search%'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function upload($fupload_name, $var = null, $width = null) {
        $vdir_upload = "./assets/files/";
        $vfile_upload = $vdir_upload . $fupload_name;
        $variable = empty($var) ? 'foto' : $var;
        $s = move_uploaded_file($_FILES["$variable"]["tmp_name"], $vfile_upload);
//        $tipe_file = $_FILES["$variable"]['type'];
//        if ($tipe_file == "image/jpg") {
//            $im_src = imagecreatefromjpeg($vfile_upload);
//            $src_width = imageSX($im_src);
//            $src_height = imageSY($im_src);
//            $dst_width = isset($width) ? $width : 505;
//            $dst_height = ($dst_width / $src_width) * $src_height;
//            $im = imagecreatetruecolor($dst_width, $dst_height);
//            imagecopyresampled($im, $im_src, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);
//            $nm = imagejpeg($im, $vdir_upload . "" . $fupload_name);
//            imagedestroy($im_src);
//            imagedestroy($im);
//        }
        if ($s == 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function unlink($file, $path = null) {
        $folder = isset($path) ? "$path/" : null;
        $cek_file = file_exists("assets/files/$folder/$file");
        if ($cek_file) {
            unlink("assets/files/$folder/$file");
        }
    }

    public function get_column($table, $data, $column = NULL) {
        $this->db->where($data);
        $sql = $this->db->get($table);
        if ($sql->num_rows()  > 0) {
            $column = $column != NULL ? $column : $table . '_id';
            return $sql->row()->$column;
        } else {
            return NULL;
        }
    }

}
