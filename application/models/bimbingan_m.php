<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Bimbingan_m extends CI_Model {

    public function dosen($id_kerja) {
        //$query = '';
        $query = "SELECT tmu.* "
                . "FROM tm_kerja INNER JOIN tm_karyawan ON (tm_karyawan.id_karyawan=tm_kerja.id_karyawan)"
                . "INNER JOIN (SELECT * FROM tm_user) AS tmu ON (tmu.id_user=tm_karyawan.id_user) where tm_kerja.id_kerja ='$id_kerja'";
        $res = $this->db->query($query);
        return $res->result();
    }

    public function bimb($id = null) {

        $query = "SELECT *, (select max(id_status) from tt_kerja_status where id_kerja = tm_kerja.id_kerja) as last FROM tm_kerja join tm_karyawan k on k.id_karyawan = tm_kerja.id_karyawan"
                . " join tt_kerja_status ttks on ttks.id_kerja = tm_kerja.id_kerja "
                . " join tm_mahasiswa tmm on tmm.id_mahasiswa = tm_kerja.id_mahasiswa"
                . " WHERE ttks.id_status=5 AND md5(tmm.id_user)='$id'";
        $res = $this->db->query($query);
        return $res->row();
    }

    public function bimb_acc($id) {
        $query = "SELECT * FROM tt_bimbingan "
                . "INNER JOIN tm_kerja ON (tt_bimbingan.id_kerja=tm_kerja.id_kerja) "
                . "INNER JOIN tt_kerja_status ON (tm_kerja.id_kerja=tt_kerja_status.id_kerja AND tt_kerja_status.id_status=5) "
                . "INNER JOIN tm_mahasiswa ON (tm_kerja.id_mahasiswa=tm_mahasiswa.id_mahasiswa) "
                . "INNER JOIN tm_user ON (tm_mahasiswa.id_user=tm_user.id_user) "
                . "INNER JOIN tm_karyawan ON (tm_karyawan.id_karyawan=tt_bimbingan.id_karyawan) "
                . "WHERE tm_karyawan.id_user = $id AND is_disetujui=0";

        $res = $this->db->query($query);
        return $res->result();
    }

}
