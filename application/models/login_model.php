<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function login($username, $password) {
        $user_clear = stripslashes(strip_tags(htmlspecialchars($username, ENT_QUOTES)));
        $pass_clear = stripslashes(strip_tags(htmlspecialchars($password, ENT_QUOTES)));
        $this->db->where("nomor", $user_clear);
        $this->db->where("passwd", $pass_clear);
        $user = $this->db->get('tm_user')->row();
        return array('user' => $user);
    }

}
