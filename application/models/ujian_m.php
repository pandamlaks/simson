<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ujian_m extends CI_Model {

    public function ujian($id = null) {
        $query = "SELECT *, (select max(id_status) from tt_kerja_status where id_kerja = tm_kerja.id_kerja) as last FROM tm_kerja JOIN tm_karyawan k ON k.id_karyawan = tm_kerja.id_karyawan"
                . " JOIN tt_kerja_status ttks ON ttks.id_kerja = tm_kerja.id_kerja "
                . " JOIN tm_mahasiswa tmm ON tmm.id_mahasiswa = tm_kerja.id_mahasiswa"
                . " WHERE ttks.id_status=6 AND md5(tmm.id_user)='$id'";
        $res = $this->db->query($query);
        return $res->row();
    }
    
    public function klik($id = null) {
        $query = "SELECT * FROM tt_ujian JOIN tm_kerja tmk ON tmk.id_kerja = tt_ujian.id_kerja "
                . " JOIN tm_karyawan k ON k.id_karyawan = tmk.id_karyawan"
                . " JOIN tt_kerja_status ttks ON ttks.id_kerja = tmk.id_kerja "
                . " JOIN tm_mahasiswa tmm ON tmm.id_mahasiswa = tmk.id_mahasiswa"
                . " WHERE ttks.id_status=6 AND (tt_ujian.id_kerja)='$id'";
        $res = $this->db->query($query);
        return $res->row();
    }

    public function dosen($id_kerja) {
        $query = "SELECT tmu.* "
                . "FROM tm_kerja INNER JOIN tm_karyawan ON (tm_karyawan.id_karyawan=tm_kerja.id_karyawan)"
                . "INNER JOIN (SELECT * FROM tm_user) AS tmu ON (tmu.id_user=tm_karyawan.id_user) where tm_kerja.id_kerja ='$id_kerja'";
        $res = $this->db->query($query);
        return $res->result();
    }

    public function ujian_acc() {
        $query = "SELECT * FROM tt_ujian "
                . "INNER JOIN tm_kerja ON (tt_ujian.id_kerja=tm_kerja.id_kerja) "
                . "INNER JOIN tt_bimbingan ON (tt_bimbingan.id_kerja=tm_kerja.id_kerja)"
                . "INNER JOIN tm_mahasiswa ON (tm_kerja.id_mahasiswa=tm_mahasiswa.id_mahasiswa) "
                . "INNER JOIN tm_user ON (tm_mahasiswa.id_user=tm_user.id_user) "
                . "INNER JOIN tm_karyawan ON (tm_karyawan.id_karyawan=tt_bimbingan.id_karyawan) "
                . "WHERE tt_ujian.is_disetujui=0 GROUP BY id_ujian";

        $res = $this->db->query($query);
        return $res->result();
    }

}
