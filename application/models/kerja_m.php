<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kerja_m extends CI_Model {

    public function show($x=null) {
        //$query = '';
        $query = "SELECT *, tmu.nama AS karyawan, tms.nama AS mahasiswa, tms.nomor AS nim  "
                . "FROM tm_kerja LEFT JOIN tm_karyawan ON (tm_karyawan.id_karyawan=tm_kerja.id_karyawan) "
                . "LEFT JOIN tm_user AS tmu ON (tmu.id_user=tm_karyawan.id_user) "
                . "LEFT JOIN tm_mahasiswa ON (tm_mahasiswa.id_mahasiswa=tm_kerja.id_mahasiswa) "
                . "LEFT JOIN tm_user AS tms ON (tms.id_user=tm_mahasiswa.id_user) "
                .$x;
//                . "INNER JOIN (SELECT id_user FROM tt_kerja_status GROUP BY id_kerja) AS tks  ON (tks.id_kerja=tm_kerja.id_kerja) "
//                . "INNER JOIN tr_status ON (tks.id_status=tr_status.id_status)";
        $res = $this->db->query($query);
        return $res->result();
    }

    public function acc($id = null) {
        //$query = '';
        $query = "SELECT * "
                . "FROM ( SELECT tm_kerja.id_kerja, MAX(id_status) AS status, tgl_eksekusi FROM tm_kerja "
                . "INNER JOIN tt_kerja_status ON (tm_kerja.id_kerja=tt_kerja_status.id_kerja) GROUP BY tm_kerja.id_kerja ) "
                . "AS tmax INNER JOIN tm_kerja ON (tm_kerja.id_kerja=tmax.id_kerja) "
                . "INNER JOIN tm_mahasiswa ON (tm_mahasiswa.id_mahasiswa=tm_kerja.id_mahasiswa) "
                . "INNER JOIN tm_user AS tmm ON (tm_mahasiswa.id_user=tmm.id_user) "
                . "INNER JOIN ( SELECT id_kerja, tgl_eksekusi AS tgl_daftar FROM tt_kerja_status "
                . "WHERE id_status=1) "
                . "AS tdaf ON (tdaf.id_kerja=tm_kerja.id_kerja) "
                . "WHERE status=1 AND tmm.jurusan='$id'";
        
        $res = $this->db->query($query);
        return $res->result();
    }
    
    public function reject($id = null) {
        //$query = '';
        $query = "SELECT * "
                . "FROM ( SELECT tm_kerja.id_kerja, MAX(id_status) AS status, tgl_eksekusi FROM tm_kerja "
                . "INNER JOIN tt_kerja_status ON (tm_kerja.id_kerja=tt_kerja_status.id_kerja) GROUP BY tm_kerja.id_kerja ) "
                . "AS tmax INNER JOIN tm_kerja ON (tm_kerja.id_kerja=tmax.id_kerja) "
                . "INNER JOIN tm_mahasiswa ON (tm_mahasiswa.id_mahasiswa=tm_kerja.id_mahasiswa) "
                . "INNER JOIN tm_user AS tmm ON (tm_mahasiswa.id_user=tmm.id_user) "
                . "INNER JOIN ( SELECT id_kerja, tgl_eksekusi AS tgl_daftar FROM tt_kerja_status "
                . "WHERE id_status=1) "
                . "AS tdaf ON (tdaf.id_kerja=tm_kerja.id_kerja) "
                . "WHERE tm_kerja.id_kerja='$id'";
        
        $res = $this->db->query($query);
        return $res->row();
    }

    public function acc2() {

        $query = "SELECT * "
                . "FROM ( SELECT tm_kerja.id_kerja, MAX(id_status) AS status, tgl_eksekusi FROM tm_kerja "
                . "INNER JOIN tt_kerja_status ON (tm_kerja.id_kerja=tt_kerja_status.id_kerja) GROUP BY tm_kerja.id_kerja ) "
                . "AS tmax INNER JOIN tm_kerja ON (tm_kerja.id_kerja=tmax.id_kerja) "
                . "INNER JOIN tm_mahasiswa ON (tm_mahasiswa.id_mahasiswa=tm_kerja.id_mahasiswa) "
                . "INNER JOIN tm_user ON (tm_mahasiswa.id_user=tm_user.id_user) "
                . "INNER JOIN ( SELECT id_kerja, tgl_eksekusi AS tgl_daftar FROM tt_kerja_status "
                . "WHERE id_status=1) "
                . "AS tdaf ON (tdaf.id_kerja=tm_kerja.id_kerja) "
                . "WHERE status=2";

        $res = $this->db->query($query);
        return $res->result();
    }

    public function permohonan($id = null) {
        $query = "SELECT *, (select max(id_status) from tt_kerja_status where id_kerja = tm_kerja.id_kerja) as last "
                . "FROM ( SELECT tm_kerja.id_kerja, MAX(id_status) AS status, tgl_eksekusi FROM tm_kerja "
                . "INNER JOIN tt_kerja_status ON (tm_kerja.id_kerja=tt_kerja_status.id_kerja) GROUP BY tm_kerja.id_kerja ) "
                . "AS tmax INNER JOIN tm_kerja ON (tm_kerja.id_kerja=tmax.id_kerja) "
                . "INNER JOIN tm_mahasiswa ON (tm_mahasiswa.id_mahasiswa=tm_kerja.id_mahasiswa) "
                . "INNER JOIN tm_user ON (tm_mahasiswa.id_user=tm_user.id_user) "
                . "INNER JOIN ( SELECT id_kerja, tgl_eksekusi AS tgl_daftar FROM tt_kerja_status "
                . "WHERE id_status=1) "
                . "AS tdaf ON (tdaf.id_kerja=tm_kerja.id_kerja) "
                . "WHERE status=3 AND md5(tm_mahasiswa.id_user)='$id'";

        $res = $this->db->query($query);
        return $res->row();
    }

    public function add_dosen($id = null) {
        $query = "SELECT *, tmm.nama AS mahasiswa, tmm.nomor AS nim "
                . "FROM ( SELECT tm_kerja.id_kerja, MAX(id_status) AS status, tgl_eksekusi FROM tm_kerja "
                . "INNER JOIN tt_kerja_status ON (tm_kerja.id_kerja=tt_kerja_status.id_kerja) GROUP BY tm_kerja.id_kerja ) "
                . "AS tmax INNER JOIN tm_kerja ON (tm_kerja.id_kerja=tmax.id_kerja) "
                . "JOIN tm_mahasiswa ON (tm_mahasiswa.id_mahasiswa=tm_kerja.id_mahasiswa) "
                . " JOIN tm_user AS tmm ON (tm_mahasiswa.id_user=tmm.id_user AND tmm.jurusan='$id') "
                . "INNER JOIN ( SELECT id_kerja, tgl_eksekusi AS tgl_daftar FROM tt_kerja_status "
                . "WHERE id_status=1) "
                . "AS tdaf ON (tdaf.id_kerja=tm_kerja.id_kerja) "
                . "WHERE status=4";
        $res = $this->db->query($query);
        return $res->result();
    }
    
    public function ganti_dosen($id = null) {
        $query = "SELECT *, tmm.nama AS mahasiswa, tmm.nomor AS nim "
                . "FROM ( SELECT tm_kerja.id_kerja, MAX(id_status) AS status, tgl_eksekusi FROM tm_kerja "
                . "INNER JOIN tt_kerja_status ON (tm_kerja.id_kerja=tt_kerja_status.id_kerja) GROUP BY tm_kerja.id_kerja ) "
                . "AS tmax INNER JOIN tm_kerja ON (tm_kerja.id_kerja=tmax.id_kerja) "
                . "JOIN tm_mahasiswa ON (tm_mahasiswa.id_mahasiswa=tm_kerja.id_mahasiswa) "
                . " JOIN tm_user AS tmm ON (tm_mahasiswa.id_user=tmm.id_user AND tmm.jurusan='$id') "
                . "INNER JOIN ( SELECT id_kerja, tgl_eksekusi AS tgl_daftar FROM tt_kerja_status "
                . "WHERE id_status=1) "
                . "AS tdaf ON (tdaf.id_kerja=tm_kerja.id_kerja) "
                . "WHERE status=5";
        $res = $this->db->query($query);
        return $res->result();
    }

    function insert() {
        $this->db->set('judul', $this->input->post('judul'));
        $this->db->set('id_konsentrasi', $this->input->post('konsentrasi'));
        $this->db->set('jenis', $this->input->post('jenis'));
        $this->db->set('tempat', $this->input->post('tempat'));
        $this->db->set('alamat', $this->input->post('alamat'));
        return $this->db->insert('tm_kerja');
    }

    function insert_status() {
        $this->db->set('jenis', $this->input->post('jenis'));
        $this->db->set('tempat', $this->input->post('tempat'));
        $this->db->set('alamat', $this->input->post('alamat'));
        return $this->db->insert('tt_kerja_status');
    }

    public function dosen($jurusan) {
        //$query = '';
        $query = "SELECT * "
                . "FROM tm_karyawan INNER JOIN tm_user ON (tm_user.id_user=tm_karyawan.id_user) where tm_user.jurusan = '$jurusan' and tm_karyawan.isPembimbingKP = 1 ORDER BY (nama) ";
//                . "INNER JOIN (SELECT id_mahasiswa FROM tt_kerja_status GROUP BY id_kerja) AS tm  ON (tks.id_mahasiswa=tm_mahasiswa.id_mahasiswa) "
//                . "INNER JOIN tr_status ON (tks.id_status=tr_status.id_status)";
        $res = $this->db->query($query);
        return $res->result();
    }

    public function rekap() {

        $query = "SELECT * "
                . "FROM ( SELECT tm_kerja.id_kerja, MAX(id_status) AS status, tgl_eksekusi FROM tm_kerja "
                . "INNER JOIN tt_kerja_status ON (tm_kerja.id_kerja=tt_kerja_status.id_kerja) GROUP BY tm_kerja.id_kerja ) "
                . "AS tmax INNER JOIN tm_kerja ON (tm_kerja.id_kerja=tmax.id_kerja) "
                . "INNER JOIN tm_mahasiswa ON (tm_mahasiswa.id_mahasiswa=tm_kerja.id_mahasiswa) "
                . "INNER JOIN tm_user ON (tm_mahasiswa.id_user=tm_user.id_user) "
                . "INNER JOIN ( SELECT id_kerja, tgl_eksekusi AS tgl_daftar FROM tt_kerja_status "
                . "WHERE id_status=1) "
                . "AS tdaf ON (tdaf.id_kerja=tm_kerja.id_kerja) "
                . "WHERE status=8 AND is_aktif=2";

        $res = $this->db->query($query);
        return $res->result();
    }

}
