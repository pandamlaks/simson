<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array("user_m"));
    }

    public function index() {
        $data['content'] = 'tambah_user_v';
        $this->load->view('index', $data);
    }

    public function profil() {
        $data['sess'] = $this->authentication_root();
        $data['arsip'] = $this->user_m->profil($data['sess']['id']);        
        $data['content'] = 'user_v';
        $this->load->view('index', $data);
    }
    
     public function edit() {
        $sess = $this->authentication_root();
        $data["nama"] = $this->input->post('nama');
        $data["nomor"] = $this->input->post('nomor');
                  
        $data["email"] = $this->input->post('email');
        $data["kontak"] = $this->input->post('kontak');        
        $this->crud_model->update_data("tm_user", $data);
        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-info-circle mg-r-md">&nbsp;&nbsp;&nbsp;&nbsp;</i>Edit profil berhasil</div>');
        redirect("user/profil");
    }
}
