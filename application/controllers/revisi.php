<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Revisi extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array("revisi_m"));
    }

    public function index() {
        $data['sess'] = $this->authentication_root();
        $data['arsip'] = $this->revisi_m->revisi($data['sess']['id']);
        if (count($data['arsip']) > 0) {
            $data['dosen'] = $this->revisi_m->dosen($data['arsip']->id_kerja);
        }
        $data['content'] = 'revisi_v';
        $this->load->view('index', $data);
    }

    public function revisi($id) {
        $sess = $this->authentication_root();
        $data['id_kerja'] = $id;
        $file = $_FILES['file']['name'];
        if (!empty($file)) {
            $lokasi_file = $_FILES['file']['tmp_name'];
            $nama_file = $_FILES['file']['name'];
            $acak = rand(000000, 999999);
            $nama_file_unik = $acak . $nama_file;
            if (!empty($lokasi_file)) {
                $upload = $this->crud_model->upload($nama_file_unik, 'file');
                if ($upload == true) {
//                    $cek = $this->crud_model->read_data("tt_ujian","id_kerja = $id")->row();
//                    if(!empty($cek->files)){
                    $this->crud_model->unlink($cek->files);
//                    }
                    $data['files'] = $nama_file_unik;
                }
            }
        }
        $this->crud_model->update_data("tt_ujian", $data, "id_kerja");
        $data2['id_kerja'] = $id;
        $data2['id_status'] = 8;
        $data2['tgl_eksekusi'] = date("Y-m-d H:i:s");
        $data2['id_user'] = $sess['users'];
        $this->crud_model->insert_data("tt_kerja_status", $data2);
        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-info-circle mg-r-md">&nbsp;&nbsp;&nbsp;&nbsp;</i>Silahkan menunggu nilai akhir dari akademik</div>');
        redirect("revisi");
    }

}
