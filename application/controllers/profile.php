<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array("profile_m"));
    }

    public function index() {
        $data['sess'] = $this->authentication_root();
        $data['row'] = $this->profile_m->system(1);
        $data['content'] = 'front/profile';
        $this->load->view('index', $data);
    }

}
