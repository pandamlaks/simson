<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kerja extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array("kerja_m"));
    }

    public function index() {

        $data['sess'] = $this->authentication_root();
        if (empty($_GET['status'])) {
            $x = "WHERE tm_kerja.is_aktif=1 OR tm_kerja.is_aktif=2";
        } else {
            $status = $_GET['status'];
            $x = "WHERE tm_kerja.is_aktif=$status";
        }

        $data['arsip'] = $this->kerja_m->show($x);
        $data['content'] = 'kerja_v';
        $this->load->view('index', $data);
    }

    public function acc() {
        $data['sess'] = $this->authentication_root();
        $data['arsip'] = $this->kerja_m->acc($data['sess']['jurusan']);
        $data['content'] = 'kerja_acc_v';
        $this->load->view('index', $data);
    }

    public function status($id, $code = null) {
        $sess = $this->authentication_root();
        $data2['id_kerja'] = $id;
        $data2['id_status'] = $code;
        $data2['tgl_eksekusi'] = date("Y-m-d H:i:s");
        $data2['id_user'] = $sess['users'];
        
        $this->crud_model->insert_data("tt_kerja_status", $data2);
        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-info-circle mg-r-md">&nbsp;&nbsp;&nbsp;&nbsp;</i>Berhasil disetujui</div>');
        redirect("kerja/acc");
    }  
        
    public function form_reject($id) {
        $data['sess'] = $this->authentication_root();
        $data['arsip'] = $this->kerja_m->reject($id);
        $data['content'] = 'reject_v';
        $this->load->view('index', $data);
    }
    
    public function reject($id, $code = null) {
        $sess = $this->authentication_root();
        $data['id_kerja'] = $id;
        $data['is_aktif'] = 4;
        $this->crud_model->update_data("tm_kerja", $data, "id_kerja");
        
        $data2['id_kerja'] = $id;
        $data2['id_status'] = $code;
        $data2['tgl_eksekusi'] = date("Y-m-d H:i:s");
        $data2['id_user'] = $sess['users'];
        $data2['alasan'] = $this->input->post("isi");
//        print_r($data2);
//        exit();
        $this->crud_model->insert_data("tt_kerja_status", $data2);
        redirect("kerja/acc");
    }   
    
    public function acc2() {
        $data['sess'] = $this->authentication_root();
        $data['arsip'] = $this->kerja_m->acc2();
        $data['content'] = 'kerja_acc_v';
        $this->load->view('index', $data);
    }

    public function status2($id, $code = null) {
        $sess = $this->authentication_root();

        $data2['id_kerja'] = $id;
        $data2['id_status'] = $code;
        $data2['tgl_eksekusi'] = date("Y-m-d H:i:s");
        $data2['id_user'] = $sess['users'];
        $this->crud_model->insert_data("tt_kerja_status", $data2);
        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-info-circle mg-r-md">&nbsp;&nbsp;&nbsp;&nbsp;</i>Sudah berhasil tercetak</div>');
        redirect("kerja/acc2");
    }

    public function permohonan() {
        $data['sess'] = $this->authentication_root();
        $data['arsip'] = $this->kerja_m->permohonan($data['sess']['id']);
        $data['content'] = 'permohonan_v';
        $this->load->view('index', $data);
    }

    public function status3($id, $code = null) {
        $sess = $this->authentication_root();

        $data2['id_kerja'] = $id;
        $data2['id_status'] = $code;
        $data2['tgl_eksekusi'] = date("Y-m-d H:i:s");
        $data2['id_user'] = $sess['users'];
        $this->crud_model->insert_data("tt_kerja_status", $data2);
        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-info-circle mg-r-md">&nbsp;&nbsp;&nbsp;&nbsp;</i>Silahkan bertemu staf jurusan untuk mendapatkan dosen pembimbing</div>');
        redirect("kerja/acc3");
    }

    public function add_dosen() {
        $data['sess'] = $this->authentication_root();
        $data['arsip'] = $this->kerja_m->add_dosen($data['sess']['jurusan']);
        $data['dosen'] = $this->kerja_m->dosen($data['sess']['jurusan']);
        $data['content'] = 'kerja_add_v';
        $this->load->view('index', $data);
    }

    public function saveset_dosen($kerja) {
        $sess = $this->authentication_root();
        $data['id_kerja'] = $kerja;
        $data['id_karyawan'] = $this->input->post("dosen");
        $data['is_aktif'] = 1;
        $this->crud_model->update_data("tm_kerja", $data, "id_kerja");
        $datas['id_kerja'] = $data['id_kerja'];
        $datas['id_status'] = 5;
        $datas['tgl_eksekusi'] = date("Y-m-d H:i:s");
        $datas['id_user'] = $sess['users'];
        $this->crud_model->insert_data("tt_kerja_status", $datas);
        redirect("kerja/add_dosen");
    }
    
    public function ganti_dosen() {
        $data['sess'] = $this->authentication_root();
        $data['arsip'] = $this->kerja_m->ganti_dosen($data['sess']['jurusan']);
        $data['dosen'] = $this->kerja_m->dosen($data['sess']['jurusan']);
        $data['content'] = 'kerja_add_v';
        $this->load->view('index', $data);
    }
    
    

    public function selesai() {
        $data['sess'] = $this->authentication_root();
        $data['arsip'] = $this->kerja_m->rekap();
        $data['content'] = 'kerja_selesai_v';
        $this->load->view('index', $data);
    }

    public function rekap($id, $code = null) {
        $sess = $this->authentication_root();
        $data['id_kerja'] = $id;
        $data['nilai'] = $this->input->post("nilai");
        $this->crud_model->insert_data("td_kinerja", $data, "id_kerja");


        $data2['id_kerja'] = $id;
        $data2['is_aktif'] = 3;
        $this->crud_model->update_data("tm_kerja", $data2, "id_kerja");

        $datas['id_kerja'] = $data['id_kerja'];
        $datas['id_status'] = $code;
        $datas['tgl_eksekusi'] = date("Y-m-d H:i:s");
        $datas['id_user'] = $sess['users'];
        $this->crud_model->insert_data("tt_kerja_status", $datas);
        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-info-circle mg-r-md">&nbsp;&nbsp;&nbsp;&nbsp;</i>Set Nilai Berhasil</div>');
        redirect("kerja/selesai");
    }

}
