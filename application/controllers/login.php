<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('login_model');
    }

    public function in_verifiy($data = null) {
        $log_in = $this->session->userdata(md5('simson'));
        if (!empty($log_in) AND $log_in == true) {
            redirect('content');
        } else {
            $this->load->view('login/login_v', $data);
        }
    }

    public function index() {
        $data['title'] = "SIMSON | Login";
        $this->in_verifiy($data);
    }

    public function verify() {
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger fade in alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');
        $this->form_validation->set_rules('user_nim', 'Username', 'trim|required');
        $this->form_validation->set_rules('user_password', 'Password', 'trim|required|callback_check');
        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $this->check();
        }
    }

    public function check() {
        $username = $this->input->post('user_nim');
        $password = $this->input->post('user_password');
        $result = $this->login_model->login($username, md5($password));

        if (count($result['user']) > 0) {
            $sess_array = array(
                'user_id' => $result['user']->id_user,
                'login' => true
            );

            $this->session->set_userdata(md5('simson'), $sess_array);
            redirect('content', 'refresh');
        } else {
            $this->session->set_flashdata('pesan', '<div class="alert alert-danger alert-dismissable mg-b-sm"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Gagal ! Silahkan coba lagi...</div>');
            redirect('login');
        }
    }

}
