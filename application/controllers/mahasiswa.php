<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array("mahasiswa_m"));
        $this->load->model('konsentrasi');
    }

//    public function index() {
//        $data['pengguna'] = $this->user_m->show();
//        $data['content'] = 'user_v';
//        $this->load->view('index', $data);
//    }

    public function index() {
        $data['sess'] = $this->authentication_root();
        $data['pengguna'] = $this->mahasiswa_m->mahasiswa();
        $data['content'] = 'mahasiswa_v';
        $this->load->view('index', $data);
    }

    public function pengawasan() {
        $data['sess'] = $this->authentication_root();
        $data['status'] = !empty($_POST)?$_POST['status']:2;
        $data['pengguna'] = $this->mahasiswa_m->pengawasan(!empty($_POST)?$_POST['status']:2);
        $data['content'] = 'mhs_pengawasan_v';
        $this->load->view('index', $data);
    }
    
    public function tambah_mhs() {
        $data['sess'] = $this->authentication_root();
        $data['content'] = 'tambah_mahasiswa_v';
        $data['konsentrasi'] = $this->konsentrasi->show();
        $data['role'] = $this->konsentrasi->role();
        $this->load->view('index', $data);
    }

    public function submit() {
        $sess = $this->authentication_root();
        $data["nama"] = $this->input->post('nama');
        $data["id_konsentrasi"] = $this->input->post('konsentrasi');
        $data["id_role"] = 6;
        $data["jurusan"] = str_replace("-", " ", $this->input->post('jurusan'));
        $data["email"] = $this->input->post('email');
        $data["nomor"] = $this->input->post('nomor');
        $data["kontak"] = $this->input->post('kontak');
        $data["passwd"] = md5($this->input->post('password'));
        $this->crud_model->insert_data("tm_user", $data);
        
        $user = $this->db->query("select * from tm_user")->last_row();
        $data2['id_user'] = $user->id_user;
        $data2['angkatan'] = $this->input->post('angkatan');
        $this->crud_model->insert_data("tm_mahasiswa", $data2);
        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-info-circle mg-r-md">&nbsp;&nbsp;&nbsp;&nbsp;</i>Pendaftaran berhasil</div>');
        redirect("mahasiswa");
    }
}
