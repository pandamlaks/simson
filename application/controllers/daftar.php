<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Daftar extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('kerja_m');
        $this->load->model('konsentrasi');
    }

    public function index() {
        $data['sess'] = $this->authentication_root();
        $data['content'] = 'tambah_kerja_v';
        $data['konsentrasi'] = $this->konsentrasi->show();
        $data['check'] = $this->db->query("select * from tm_kerja where id_mahasiswa = " . $data['sess']['mahasiswa'])->row();
        $this->load->view('index', $data);
    }

    public function input() {
        $data['sess'] = $this->authentication_root();
        if ($this->input->post('submit')):
            $this->Kerja_m->insert();
        endif;
        $data['content'] = 'tambah_kerja_v';
        $this->load->view('index', $data);
    }

    public function submit() {
        $sess = $this->authentication_root();
        $mhs = $this->db->query("select * from tm_mahasiswa where md5(id_user) = '$sess[id]'")->row();
        $data["id_mahasiswa"] = $mhs->id_mahasiswa;
        $data["id_konsentrasi"] = $this->input->post('konsentrasi');
        $data["jenis"] = KP;
        $data["judul"] = $this->input->post('judul');
        $data["tempat"] = $this->input->post('tempat');
        $data["alamat"] = $this->input->post('alamat');
        $this->crud_model->insert_data("tm_kerja", $data);

        $data2["id_kerja"] = $this->db->insert_id();
        $data2["id_status"] = 1;
        $data2["tgl_eksekusi"] = Date('Y-m-d H:i:s');
        $data2["id_user"] = $mhs->id_user;
        $this->crud_model->insert_data("tt_kerja_status", $data2);
        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-info-circle mg-r-md">&nbsp;&nbsp;&nbsp;&nbsp;</i>Pendaftaran berhasil</div>');
        redirect("daftar");
    }

    public function status() {
        $sess = $this->authentication_root();
        $mhs = $this->db->query("select * from tm_mahasiswa where md5(id_user) = '$sess[id]'")->row();
        $data["id_kerja"] = $krj->id_kerja;
        $data["id_kerja"] = $this->input->post('id_kerja');
        $data["id_status"] = $this->input->post('status');
        $data["tgl_eksekusi"] = $this->input->post('tanggal');
        $data["id_user"] = $this->input->post('user');
        $this->crud_model->insert_data("tt_kerja_status", $data);
        redirect("kerja");
    }

    public function user() {
        $data['sess'] = $this->authentication_root();
        $data['content'] = 'tambah_user_v';
        $data['konsentrasi'] = $this->konsentrasi->show();
        $data['role'] = $this->konsentrasi->role();
        $this->load->view('index', $data);
    }

    public function submit_user() {
        $sess = $this->authentication_root();
        $data["nama"] = $this->input->post('nama');
        $data["id_konsentrasi"] = $this->input->post('konsentrasi');
        $data["id_role"] = 6;
        $data["jurusan"] = str_replace("-", " ", $this->input->post('jurusan'));
        $data["email"] = $this->input->post('email');
        $data["nomor"] = $this->input->post('nomor');
        $data["kontak"] = $this->input->post('kontak');
        $data["passwd"] = md5($this->input->post('password'));
        $this->crud_model->insert_data("tm_user", $data);

        $user = $this->db->query("select * from tm_user")->last_row();
        $data2['id_user'] = $user->id_user;
        $data2['angkatan'] = $this->input->post('angkatan');
        $this->crud_model->insert_data("tm_mahasiswa", $data2);
        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-info-circle mg-r-md">&nbsp;&nbsp;&nbsp;&nbsp;</i>Pendaftaran berhasil</div>');
        redirect("daftar/user");
    }

    public function konsentrasi($konsen) {
        $data['sess'] = $this->authentication_root();
        $data['val_kons'] = $konsen;
        if ($data['val_kons'] != null) {
            $data['konsentrasi'] = $this->konsentrasi->show($konsen);
        }
        $this->load->view('konsentrasi', $data);
    }

    public function submit_user2() {
        $sess = $this->authentication_root();
        $data["nama"] = $this->input->post('nama');
        $data["id_konsentrasi"] = $this->input->post('konsentrasi');
        $data["id_role"] = $this->input->post('role');
        $data["jurusan"] = str_replace("-", " ", $this->input->post('jurusan'));
        $data["email"] = $this->input->post('email');
        $data["nomor"] = $this->input->post('nomor');
        $data["kontak"] = $this->input->post('kontak');
        $data["passwd"] = md5($this->input->post('password'));
        $this->crud_model->insert_data("tm_user", $data);
        $user = $this->db->query("select * from tm_user")->last_row();
        $data2['id_user'] = $user->id_user;
        $data2['gelar'] = $this->input->post('gelar');
        $this->crud_model->insert_data("tm_karyawan", $data2);
        redirect("daftar/user");
    }

}
