<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ujian extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array("ujian_m"));
    }

    public function index() {
        $data['sess'] = $this->authentication_root();
        $data['arsip'] = $this->ujian_m->ujian($data['sess']['id']);
        if (count($data['arsip']) > 0){
        $data['dosen'] = $this->ujian_m->dosen($data['arsip']->id_kerja);
        $data['klik'] = $this->ujian_m->klik($data['arsip']->id_kerja);
        }
        $data['content'] = 'ujian_v';
        $this->load->view('index', $data);
    }
    
     public function ujian() {
        $sess = $this->authentication_root();
        $kerja = $this->ujian_m->ujian($sess['id']);
        $data['id_kerja'] = $kerja->id_kerja;
        $data['jenis'] = $kerja->jenis;
        $data['waktu'] = date("Y-m-d H:i:s");
        $data['is_disetujui'] = 0;        
        $data['klik'] = 1;        
        $this->crud_model->insert_data("tt_ujian", $data, "id_kerja");
        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-info-circle mg-r-md">&nbsp;&nbsp;&nbsp;&nbsp;</i>Request berhasil</div>');
        redirect("ujian");
    }
    
    public function acc() {
        $data['sess'] = $this->authentication_root();        
        $data['arsip'] = $this->ujian_m->ujian_acc();
        $data['content'] = 'ujian_acc_v';
        $this->load->view('index', $data);
    }

    public function status($id, $status, $tgl) {
        $sess = $this->authentication_root();
        $data['id_kerja'] = $id;
//        $data['is_aktif'] = $code;
//        $this->crud_model->update_data("tm_kerja", $data, "id_kerja");
        $data['wkt_dijadwalkan'] = date("Y-m-d", strtotime($tgl));
         $data['id_user_setuju'] = $sess['users'];
        $data['is_disetujui'] = $status;
        $this->crud_model->update_data("tt_ujian", $data, "id_kerja");
        $data2['id_kerja'] = $id;
        $data2['id_status'] = 7;
        $data2['tgl_eksekusi'] = date("Y-m-d H:i:s");
        $data2['id_user'] = $sess['users'];
        $this->crud_model->insert_data("tt_kerja_status", $data2);
        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-info-circle mg-r-md">&nbsp;&nbsp;&nbsp;&nbsp;</i>Jadwal berhasil diset</div>');
        redirect("ujian/acc");
    }
}
