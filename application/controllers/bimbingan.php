<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Bimbingan extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array("bimbingan_m"));
    }

    public function index() {
        $data['sess'] = $this->authentication_root();
        $data['arsip'] = $this->bimbingan_m->bimb($data['sess']['id']);
        if (count($data['arsip']) > 0){
            $data['dosen'] = $this->bimbingan_m->dosen($data['arsip']->id_kerja);
        }
        $iduser = $data['sess']['id'];
        $data['cek'] = $this->db->query("select * from tt_bimbingan b join tm_kerja k on k.id_kerja = b.id_kerja join tm_mahasiswa m on m.id_mahasiswa = k.id_mahasiswa where md5(m.id_user) = '$iduser'")->last_row();
        $data['content'] = 'bimbingan_v';
//        print_r($data['cek']);
//        exit();
        $this->load->view('index', $data);
    }

    public function bimbingan() {
        
        $sess = $this->authentication_root();
        $kerja = $this->bimbingan_m->bimb($sess['id']);
        $data['id_kerja'] = $kerja->id_kerja;
        $data['id_karyawan'] = $kerja->id_karyawan;
        $data['waktu'] = date("Y-m-d H:i:s", strtotime($this->input->post('tanggal')));
//        $data['waktu'] = $this->input->post('tanggal');
        $data['is_disetujui'] = 0;
        $data['keterangan'] = $this->input->post('materi');
        $this->crud_model->insert_data("tt_bimbingan", $data, "id_kerja");
        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-info-circle mg-r-md">&nbsp;&nbsp;&nbsp;&nbsp;</i>Request berhasil</div>');
        redirect("bimbingan");
    }

    public function acc() {
        $data['sess'] = $this->authentication_root();        
        $data['arsip'] = $this->bimbingan_m->bimb_acc($data['sess']['users']);
        $data['content'] = 'kerja_acc_v';
        $this->load->view('index', $data);
    }

    public function status($id, $kerja, $status = null) {
        $sess = $this->authentication_root();
        $data['id_bimbingan'] = $id;
        $data['is_disetujui'] = $status;
        $this->crud_model->update_data("tt_bimbingan", $data, "id_bimbingan");
        
        $data1['id_kerja'] = $kerja;
        $data1['is_aktif'] = 2;
        $this->crud_model->update_data("tm_kerja", $data1, "id_kerja");
        
        $data2['id_kerja'] = $kerja;
        $data2['id_status'] = 6;
        $data2['tgl_eksekusi'] = date("Y-m-d H:i:s");
        $data2['id_user'] = $sess['users'];
        $this->crud_model->insert_data("tt_kerja_status", $data2);
        redirect("bimbingan/acc");
    }

}