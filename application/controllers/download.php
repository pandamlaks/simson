<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Download extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $data['sess'] = $this->authentication_root();
        $data['content'] = 'download';
        $this->load->view('index', $data);
    }

}
