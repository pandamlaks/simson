<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Content extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array("content_m"));
    }

    public function index($id = null) {
        $data['sess'] = $this->authentication_root();
        $data['footer'] = 1;
        if (empty($id)) {
            $data['berita'] = $this->content_m->show();
            $data['document'] = $this->content_m->document();
            $data['content'] = 'front/content';
            $this->load->view('index', $data);
        } else {
            $data['row'] = $this->content_m->detail($id);
            $data['document'] = $this->content_m->document();
            $data['content'] = 'front/content_detail';
            $this->load->view('index', $data);
        }
    }
    public function upload(){
        $data['sess'] = $this->authentication_root();
        $data['content'] = 'front/content_upload';
        $this->load->view('index', $data);
    }
    
    public function submit_content() {
        $sess = $this->authentication_root();
        $data["id_karyawan"] = $sess['karyawan'];
        $data["judul"] = $this->input->post('judul');
        $data["tanggal"] = date("Y-m-d H:i:s");
        $file = $_FILES['gambar']['name'];
        if (!empty($file)) {
            $lokasi_file = $_FILES['gambar']['tmp_name'];
            $nama_file = $_FILES['gambar']['name'];
            $acak = rand(000000, 999999);
            $nama_file_unik = $acak . $nama_file;
            if (!empty($lokasi_file)) {
                $upload = $this->crud_model->upload($nama_file_unik, 'gambar');
                if ($upload == true) {
//                    $cek = $this->crud_model->read_data("tt_ujian","id_kerja = $id")->row();
//                    if(!empty($cek->files)){
                    $this->crud_model->unlink($cek->gambar);
//                    }
                    $data['gambar'] = $nama_file_unik;
                }
            }
        }
//        echo $nama_file_unik;
//        exit();
//        $data["gambar"] = 
        $data["isi"] = $this->input->post('isi');
//        $data["lampiran"] =         
        $this->crud_model->insert_data("content", $data);
        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-info-circle mg-r-md">&nbsp;&nbsp;&nbsp;&nbsp;</i>Berita berhasil ditambahkan</div>');
        redirect("content/upload");
    }

}
