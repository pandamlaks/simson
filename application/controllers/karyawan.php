<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array("karyawan_m"));
    }

    public function index() {
        $data['sess'] = $this->authentication_root();
        $data['dosen'] = $this->karyawan_m->karyawan();
        $data['content'] = 'karyawan_v';
        $this->load->view('index', $data);
    }
    
    public function set_bimbing($karyawan) {
        $sess = $this->authentication_root();
        $data['id_karyawan'] = $karyawan;
        $data['isPembimbingKP'] = $this->input->post("bimbingKP");
        $data['isPembimbingTA'] = $this->input->post("bimbingTA");
        $this->crud_model->update_data("tm_karyawan", $data, "id_karyawan");
        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-info-circle mg-r-md">&nbsp;&nbsp;&nbsp;&nbsp;</i>Berhasil diset</div>');
        redirect("karyawan");
    }

}
