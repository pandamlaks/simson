<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Proses_kp extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array("proses_kp_m"));
    }
    
    public function index() {
        $data['sess'] = $this->authentication_root();
        $data['proses'] = $this->proses_kp_m->kp($data['sess']['id']);
        $data['content'] = 'proses_kp_v';
        $this->load->view('index', $data);
    }
}