<!DOCTYPE html>
<html>
    <head>

        <!-- Title -->
        <title>SIMKP</title>

        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta charset="UTF-8">
        <meta name="description" content="Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="LAX" />

        <!-- Styles -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
        <link href="<?php echo base_url() ?>assets/plugins/pace-master/themes/blue/pace-theme-flash.css" rel="stylesheet"/>
        <link href="<?php echo base_url() ?>assets/plugins/uniform/css/uniform.default.min.css" rel="stylesheet"/>
        <link href="<?php echo base_url() ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>assets/plugins/fontawesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>assets/plugins/line-icons/simple-line-icons.css" rel="stylesheet" type="text/css"/>	
        <link href="<?php echo base_url() ?>assets/plugins/offcanvasmenueffects/css/menu_cornerbox.css" rel="stylesheet" type="text/css"/>	
        <link href="<?php echo base_url() ?>assets/plugins/waves/waves.min.css" rel="stylesheet" type="text/css"/>	
        <link href="<?php echo base_url() ?>assets/plugins/switchery/switchery.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>assets/plugins/3d-bold-navigation/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>assets/plugins/slidepushmenus/css/component.css" rel="stylesheet" type="text/css"/>

        <link href="<?php echo base_url() ?>assets/plugins/weather-icons-master/css/weather-icons.min.css" rel="stylesheet" type="text/css"/>	
        <link href="<?php echo base_url() ?>assets/plugins/metrojs/MetroJs.min.css" rel="stylesheet" type="text/css"/>	
        <link href="<?php echo base_url() ?>assets/plugins/toastr/toastr.min.css" rel="stylesheet" type="text/css"/>	

        <!--         Styles 
                <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
                <link href="<?php echo base_url() ?>assets/plugins/pace-master/themes/blue/pace-theme-flash.css" rel="stylesheet"/>
                <link href="<?php echo base_url() ?>assets/plugins/uniform/css/uniform.default.min.css" rel="stylesheet"/>
                <link href="<?php echo base_url() ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
                <link href="<?php echo base_url() ?>assets/plugins/fontawesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
                <link href="<?php echo base_url() ?>assets/plugins/line-icons/simple-line-icons.css" rel="stylesheet" type="text/css"/>	
                <link href="<?php echo base_url() ?>assets/plugins/offcanvasmenueffects/css/menu_cornerbox.css" rel="stylesheet" type="text/css"/>	
                <link href="<?php echo base_url() ?>assets/plugins/waves/waves.min.css" rel="stylesheet" type="text/css"/>	
                <link href="<?php echo base_url() ?>assets/plugins/switchery/switchery.min.css" rel="stylesheet" type="text/css"/>
                <link href="<?php echo base_url() ?>assets/plugins/3d-bold-navigation/css/style.css" rel="stylesheet" type="text/css"/>
                <link href="<?php echo base_url() ?>assets/plugins/slidepushmenus/css/component.css" rel="stylesheet" type="text/css"/>
                <link href="<?php echo base_url() ?>assets/plugins/dropzone/dropzone.min.css" rel="stylesheet" type="text/css"/>
                <link href="<?php echo base_url() ?>assets/plugins/weather-icons-master/css/weather-icons.min.css" rel="stylesheet" type="text/css"/>	
                <link href="<?php echo base_url() ?>assets/plugins/metrojs/MetroJs.min.css" rel="stylesheet" type="text/css"/>	
                <link href="<?php echo base_url() ?>assets/plugins/toastr/toastr.min.css" rel="stylesheet" type="text/css"/>-->
        <!-- Theme Styles -->
        <link href="<?php echo base_url() ?>assets/css/modern.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>assets/css/themes/blue.css" class="theme-color" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>assets/css/custom.css" rel="stylesheet" type="text/css"/>



        <script src="<?php echo base_url() ?>assets/plugins/3d-bold-navigation/js/modernizr.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/offcanvasmenueffects/js/snap.svg-min.js"></script>
        <link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap-material-datetimepicker.css" />
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!-- DataTables CSS -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/jquery.dataTables.css"/>

        <!-- jQuery -->
        <script type="text/javascript" charset="utf8" src="<?php echo base_url() ?>assets/js/jquery-1.10.2.min.js"></script>

        <!-- DataTables -->
        <script type="text/javascript" charset="utf8" src="<?php echo base_url() ?>assets/js/jquery.dataTables.js"></script>
        <style>
            .form-control-wrapper 
            {
                margin: 10px 20px;
            }
            code 
            {
                padding: 10px;
                background: #eee!important;
                display: block;
                color: #000;
            }
            code > p 
            {
                font-weight: bold;
                color: #000;
                font-size: 1.5em;
            }
        </style>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body class="page-header-fixed page-sidebar-fixed small-sidebar">
        <div class="overlay"></div>

        <div class="menu-wrap">
            <nav class="profile-menu">
            </nav>
            <button class="close-button" id="close-button">Close Menu</button>
        </div>
        <form class="search-form" action="#" method="GET">
            <div class="input-group">
                <input type="text" name="search" class="form-control search-input" placeholder="Search...">
                <span class="input-group-btn">
                    <button class="btn btn-default close-search waves-effect waves-button waves-classic" type="button"><i class="fa fa-times"></i></button>
                </span>
            </div><!-- Input Group -->
        </form><!-- Search Form -->
        <main class="page-content content-wrap">
            <div class="navbar">
                <div class="navbar-inner">
                    <div class="sidebar-pusher">
                        <a href="javascript:void(0);" class="waves-effect waves-button waves-classic push-sidebar">
                            <i class="fa fa-bars"></i>
                        </a>
                    </div>
                    <div class="logo-box">
                        <a href="<?php echo base_url() ?>content" class="logo-text"><span>SIMKP</span></a>
                    </div><!-- Logo Box -->
                    <div class="search-button">
                        <a href="javascript:void(0);" class="waves-effect waves-button waves-classic show-search"><i class="fa fa-search"></i></a>
                    </div>
                    <div class="topmenu-outer">
                        <div class="top-menu">
                            <ul class="nav navbar-nav navbar-left">
                                <li>		
                                    <a href="javascript:void(0);" class="waves-effect waves-button waves-classic sidebar-toggle"><i class="fa fa-bars"></i></a>
                                </li>
                                <!--<li>
                                    <a href="#cd-nav" class="waves-effect waves-button waves-classic cd-nav-trigger"><i class="fa fa-diamond"></i></a>
                                </li> -->
                                <li>		
                                    <a href="javascript:void(0);" class="waves-effect waves-button waves-classic toggle-fullscreen"><i class="fa fa-expand"></i></a>
                                </li>

                            </ul>

                            <ul class="nav navbar-nav navbar-right">                         
                                <?php if ($sess['login'] != md5(true)) { ?>
                                    <li>
                                        <a href="<?php echo base_url() ?>daftar/user" class="waves-effect waves-button waves-classic">
                                            Daftar Baru<i class="fa fa-user-plus m-l-xs"></i>
                                        </a>
                                    </li>    
                                    <li>
                                        <a href="<?php echo base_url() ?>login" class="log-out waves-effect waves-button waves-classic">
                                            Login<i class="fa fa-sign-in m-l-xs"></i>
                                        </a>
                                    </li>                                                                        
                                <?php } else { ?>                                   
                                    <li>	
                                        <a href="<?php echo base_url() ?>kerja" class="waves-effect waves-button waves-classic">
                                            Cari Judul KP<i class="fa fa-search m-l-sm"></i></a>
                                    </li>
                                    <li>

                                        <a href="<?php echo base_url() ?>user/profil" class="userbox waves-button waves-classic">
                                            <?php echo $sess['nama'] ?>
                                            ( <?php echo $sess['level_role'] ?> )
                                            <i class="fa fa-user m-l-xs"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url() ?>logout" class="log-out waves-effect waves-button waves-classic">
                                            Logout<i class="fa fa-sign-out m-l-xs"></i>
                                        </a>
                                    </li>
                                <?php } ?>

                            </ul><!-- Nav -->
                        </div><!-- Top Menu -->
                    </div>
                </div>
            </div><!-- Navbar -->
            <div class="page-sidebar sidebar">
                <div class="page-sidebar-inner slimscroll">

                    <ul class="menu accordion-menu">
                        <li>
                            <?php if ($sess['id_role'] == 1) { ?>
                            <li><a href="<?php echo base_url() ?>mahasiswa" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-list-alt"></span><p>List Mahasiswa</p></a></li>
                            <li><a href="<?php echo base_url() ?>karyawan" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-list-alt"></span><p>List Dosen</p></a></li>                                                      

                        <?php } elseif ($sess['id_role'] == 2) { ?>
                            <li><a href="<?php echo base_url() ?>kerja/acc" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-check"></span><p>Persetujuan Proposal</p></a></li>
                            <li><a href="<?php echo base_url() ?>mahasiswa/pengawasan" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-tasks"></span><p>Monitoring</p></a></li>
                            <li><a href="<?php echo base_url() ?>ujian/acc" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-tasks"></span><p>Open House KP</p></a></li>                            
                            <li><a href="<?php echo base_url() ?>karyawan" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-list-alt"></span><p>List Dosen</p></a></li>
                            <li><a href="<?php echo base_url() ?>kerja" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-search"></span><p>Cari KP</p></a></li>

                        <?php } elseif ($sess['id_role'] == 3) { ?>
                            <li><a href="<?php echo base_url() ?>kerja/acc" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-check"></span><p>Persetujuan Proposal</p></a></li>
                            <li><a href="<?php echo base_url() ?>bimbingan/acc" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-tasks"></span><p>Bimbingan</p></a></li>
                            <li><a href="<?php echo base_url() ?>ujian/acc" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-tasks"></span><p>Open House KP</p></a></li>
                            <li><a href="<?php echo base_url() ?>kerja/selesai" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-file"></span><p>Rekap Nilai Akhir</p></a></li>
                            <li><a href="<?php echo base_url() ?>kerja" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-search"></span><p>Cari KP</p></a></li>

                        <?php } elseif ($sess['id_role'] == 4) { ?>
                            <li><a href="<?php echo base_url() ?>kerja/add_dosen" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-tasks"></span><p>Set DosPem</p></a></li>
                            <li><a href="<?php echo base_url() ?>kerja/ganti_dosen" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-tasks"></span><p>Ganti DosPem</p></a></li>
                            <li><a href="<?php echo base_url() ?>mahasiswa/pengawasan" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-eye-open"></span><p>Monitoring</p></a></li>
                            <li><a href="<?php echo base_url() ?>mahasiswa" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-list-alt"></span><p>List Mahasiswa</p></a></li>
                            <li><a href="<?php echo base_url() ?>karyawan" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-list-alt"></span><p>List Dosen</p></a></li>
                            <li><a href="<?php echo base_url() ?>daftar/user" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-plus"></span><p>Tambah User</p></a></li>
                            <li><a href="<?php echo base_url() ?>content/upload" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-upload"></span><p>Upload Berita</p></a></li>

                        <?php } elseif ($sess['id_role'] == 5) { ?>
                            <li><a href="<?php echo base_url() ?>kerja/acc2" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-print"></span><p>Cetak Surat Permohonan</p></a></li>
                            <li><a href="<?php echo base_url() ?>kerja/selesai" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-file"></span><p>Rekap Nilai Akhir</p></a></li>
                            <li><a href="<?php echo base_url() ?>kerja" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-search"></span><p>Cari KP</p></a></li>
                            <li><a href="<?php echo base_url() ?>content/upload" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-upload"></span><p>Upload Berita</p></a></li>

                        <?php } elseif ($sess['id_role'] == 6) { ?>
                            <li><a href="<?php echo base_url() ?>daftar" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-pencil"></span><p>1. Pendaftaran</p></a></li>
                            <li><a href="<?php echo base_url() ?>kerja/permohonan" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-edit"></span><p>2. Permohonan</p></a></li>
                            <li><a href="<?php echo base_url() ?>bimbingan" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-tasks"></span><p>3. Bimbingan</p></a></li>
                            <li><a href="<?php echo base_url() ?>ujian" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-file"></span><p>4. Open House KP</p></a></li>
                            <li><a href="<?php echo base_url() ?>revisi" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-edit"></span><p>5. Revisi</p></a></li>                            
                            <li><a href="<?php echo base_url() ?>kerja" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-search"></span><p>Cari KP</p></a></li>
                            <!--<li><a href="<?php echo base_url() ?>download" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-download"></span><p>Download</p></a></li>-->

                        <?php } else { ?>
                            <li><a href="<?php echo base_url() ?>content" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-home"></span><p>Home</p></a></li>
                            <li><a href="<?php echo base_url() ?>kerja" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-search"></span><p>Cari KP</p></a></li>
                            <!--<li><a href="<?php echo base_url() ?>download" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-download"></span><p>Download</p></a></li>-->
                        <?php } ?>
                    </ul>
                </div><!-- Page Sidebar Inner -->
            </div><!-- Page Sidebar -->
            <div class="page-inner">
                <div id="main-wrapper">
                    <?php $this->load->view($content) ?>
                </div><!-- Main Wrapper -->
                <?php
                if (!empty($footer)) {
                    echo 1;
                    $this->load->view("extend/footer");
                }
                ?>
            </div><!-- Page Inner -->
        </main><!-- Page Content -->

        <!-- Javascripts -->
        <script src="<?php echo base_url() ?>assets/plugins/jquery/jquery-2.1.3.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/jquery-ui/jquery-ui.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/pace-master/pace.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/jquery-blockui/jquery.blockui.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>

        <script type="text/javascript" src="<?php echo base_url() ?>js/material.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/bootstrap-material-datetimepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function ()
            {
                $('.date-format').bootstrapMaterialDatePicker
                        ({
                            format: 'DD-MM-YYYY',
                            time: false

                        });
                $('.datetime-format').bootstrapMaterialDatePicker
                        ({
                            format: 'DD-MM-YYYY HH:mm',
                            time: true

                        });
            });
        </script>

        <script src="<?php echo base_url() ?>assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/switchery/switchery.min.js"></script>

        <script src="<?php echo base_url() ?>assets/plugins/uniform/jquery.uniform.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/pages/form-wizard.js"></script>

        <script src="<?php echo base_url() ?>assets/plugins/offcanvasmenueffects/js/classie.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/offcanvasmenueffects/js/main.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/waves/waves.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/3d-bold-navigation/js/main.js"></script>

        <script src="<?php echo base_url() ?>assets/plugins/waypoints/jquery.waypoints.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/jquery-counterup/jquery.counterup.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/toastr/toastr.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/flot/jquery.flot.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/flot/jquery.flot.time.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/flot/jquery.flot.symbol.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/flot/jquery.flot.resize.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/flot/jquery.flot.tooltip.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/curvedlines/curvedLines.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/metrojs/MetroJs.min.js"></script>

        <script src="<?php echo base_url() ?>assets/js/modern.js"></script>

        <script src="<?php echo base_url() ?>assets/js/pages/dashboard.js"></script>

        <script src="<?php echo base_url() ?>assets/plugins/jquery-mockjax-master/jquery.mockjax.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/moment/moment.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/datatables/js/jquery.datatables.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/x-editable/bootstrap3-editable/js/bootstrap-editable.js"></script>
        <script src="<?php echo base_url() ?>assets/js/pages/table-data.js"></script>




    </body>
</html>