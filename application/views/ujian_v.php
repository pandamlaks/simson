<link href="<?php echo base_url() ?>assets/plugins/dropzone/dropzone.min.css" rel="stylesheet" type="text/css"/>
<div class="page-title">
    <h3>Request Ujian</h3>                    
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    <?php echo $this->session->flashdata('message'); ?>
                     <?php if (empty($arsip)) { ?>
                        data belum ada
                    <?php } elseif ($arsip->last == 9) { ?>
                        Anda sudah menyelesaikan KP.
                    <?php } else { ?>
                        <div id="rootwizard">
                            <form id="wizardForm" method="post" action="<?php echo base_url() ?>ujian/ujian">
                                <div class="tab-content">
                                    <div class="tab-pane active fade in" id="tab1">
                                        <div class="row m-b-lg">
                                            <div class="col-md-6 center">
                                                <div class="row center">

                                                    <div class="form-group col-md-12">
                                                        <label for="exampleInputName">Judul KP/TA : </label>                                          
                                                        <input type="text" class="form-control" id="input-disabled" value="<?php echo $arsip->judul ?>" disabled>                                                        
                                                    </div>

                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="exampleInputName">Jenis : </label>
                                                    <input type="text" class="form-control" id="input-disabled" value="<?php echo $arsip->jenis; ?>" disabled>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label for="exampleInputName">Dosen Pembimbing : </label>
                                                    <input type="text" class="form-control" id="input-disabled" value="<?php
                                                    foreach ($dosen as $number => $kerja1) {
                                                        echo $kerja1->nama;
                                                    }
                                                    ?>" disabled>
                                                </div>


                                                <!--                                            <div class="col-md-12">
                                                                                                <div class="panel panel-white">
                                                                                                    <div class="panel-body">
                                                                                                        <label for="exampleInputName">Upload Surat Penilaian Perusahaan : </label>
                                                                                                        <form action="/file-upload" class="dropzone">
                                                                                                            <div class="fallback">
                                                                                                                <input name="file" type="file" multiple />
                                                                                                            </div>
                                                                                                        </form>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>-->

                                                <div class="btn col-md-6">
                                                    <button type="submit" <?php echo!empty($klik->klik) ? $klik->klik == 1 ? "disabled" : null : null; ?> class="btn btn-success" name="submit" value="simpan">Submit Request</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>  
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>