
<div id="main-wrapper">
    
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">

                <div class="panel-body">
                    <div class="table-responsive">
                        
                        <table id="table_id" class="display table" style="width: 100%; cellspacing: 0;">
                            <thead>
                                <tr>
                                    
                                    <th>Judul</th>
                                    <th>Tempat</th>
                                    <th>Nilai</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    
                                    <th>Judul</th>
                                    <th>Tempat</th>
                                    <th>Nilai</th>
                                    <th>Aksi</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php
                                foreach ($arsip as $row) {
                                    ?>
                                
                                    <tr>
                                        <form action="<?php echo base_url() ?>kerja/rekap/<?php echo $row->id_kerja ?>/9" method="post">
                                        
                                        <td> <?php echo $row->judul ?> <br>
                                            <?php echo $row->nomor ?> <?php echo $row->nama ?> </td>
                                        <td> <?php echo $row->tempat ?> <br>
                                            <?php echo $row->alamat ?> </td>
                                        <td><select class="select2-dropdown-open" aria-controls="example" name="nilai">
                                              
                                                <option value="A">A</option>
                                                <option value="A-">A-</option>
                                                <option value="A/B">A/B</option>
                                                <option value="B">B</option>
                                                <option value="B-">B-</option>
                                                <option value="B/C">B/C</option>
                                                <option value="C">C</option>
                                                <option value="C-">C-</option>
                                                <option value="D">D</option>
                                                <option value="E">E</option>
                                            </select></td>
                                            <td><button type="submit" value="simpan"><i class="fa fa-check"></i></button></td>
                                        </form>
                                </tr>
                            
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url() ?>assets/plugins/jquery/jquery-2.1.3.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/js/jquery.datatables.min.js"></script>
<script>
        $('#table_id').DataTable();
</script>