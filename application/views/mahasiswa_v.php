<div id="main-wrapper">
    
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title">Daftar Mahasiswa</h4>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">                        
                        <div><a  href="<?php echo base_url() ?>mahasiswa/tambah_mhs" type="button" class="btn btn-success btn-addon m-b-sm"><i class="fa fa-plus"></i> Tambah User</a></div>
                        <table id="table_id" class="display table" style="width: 100%; cellspacing: 0;">
                            <thead>
                                <tr>
                                    <th>Nomor</th>
                                    <th>Nama</th>
                                    <th>Jurusan</th>
                                    <th>Angkatan</th>
                                    <th>Kontak</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Nomor</th>
                                    <th>Nama</th>
                                    <th>Jurusan</th>
                                    <th>Angkatan</th>
                                    <th>Kontak</th>
                                    <th>Status</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php foreach ($pengguna as $row) { ?>
                                    <tr>
                                        <td> <?php echo $row->nomor ?> </td>
                                        <td> <?php echo $row->nama ?> </td>
                                        <td> <?php echo $row->jurusan ?> </td>
                                        <td> <?php echo $row->angkatan ?> </td>
                                        <td> <?php echo $row->kontak ?> </td>
                                        <td> <?php
                                            if ($row->is_aktif == 0) {
                                                echo "Belum Aktif";
                                            } elseif ($row->is_aktif == 1) {
                                                echo "Aktif";
                                            } else {
                                                echo "Selesai";
                                            }
                                            ?> </td>

                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script src="<?php echo base_url() ?>assets/plugins/jquery/jquery-2.1.3.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/js/jquery.datatables.min.js"></script>
<script>
    $('#table_id').DataTable();
</script>