<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">                     
                <div class="panel-body">                 
                    <div class="table-responsive">
<!--                        <h2>ARSIP KP</h2>-->
                        <div id="example_length" class="dataTables_length">
                            <b>ARSIP KP</b>
                            <form action="" method="get">
                                <label>Lihat Berdasarkan <select class="" aria-controls="example" name="status">
                                        <option value="">KP Mahasiswa Aktif</option>
                                        <option value="2">KP Mahasiswa In Progres</option>
                                        <option value="3">KP Mahasiswa Selesai</option>
                                        <!--<option value="4">KP Ditolak</option>-->
                                        
                                    </select>
                                </label>                                                                
                                <button type="submit" class="btn btn-success"><i class="fa fa-search"></i></button>                                
                            </form>
                        </div>
                        <table id="table_id" class="display table" style="width: 100%; cellspacing: 0;">
                            <thead>
                                <tr>
                                    
                                    <th>Judul</th>
                                    <th>Tempat</th>                                    
                                    <th>Pembimbing</th>
                                    <th>Status</th>

                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    
                                    <th>Judul</th>
                                    <th>Tempat</th>                                    
                                    <th>Pembimbing</th>
                                    <th>Status</th>

                                </tr>
                            </tfoot>
                            <tbody>
                                <?php
                                foreach ($arsip as $row) {
                                    ?>
                                    <tr>
                                        
                                        <td> <?php echo $row->judul ?> <br>
                                            <?php echo $row->nim ?> <?php echo $row->mahasiswa ?> </td>
                                        <td> <?php echo $row->tempat ?> <br>
                                            <?php echo $row->alamat ?> </td>
                                        <td> <?php echo $row->karyawan ?>, <?php echo $row->gelar ?> </td>
                                        <td> <?php
                                            if ($row->is_aktif == 0) {
                                                echo "Belum Aktif";
                                            } elseif ($row->is_aktif == 1) {
                                                echo "Aktif";
                                            } elseif ($row->is_aktif == 2) {
                                                echo "In Progress";
                                            } elseif ($row->is_aktif == 3) {
                                                echo "Selesai";
                                            } else {
                                                echo "Tolak";
                                            }
                                            ?>
                                        <?php } ?>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="<?php echo base_url() ?>assets/plugins/jquery/jquery-2.1.3.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/js/jquery.datatables.min.js"></script>
<script>
    $('#table_id').DataTable();
</script>


