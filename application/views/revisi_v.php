<link href="<?php echo base_url() ?>assets/plugins/dropzone/dropzone.min.css" rel="stylesheet" type="text/css"/>
<div class="page-title">
    <h3>Upload Revisi</h3>                    
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    <?php echo $this->session->flashdata('message'); ?>
                    <?php if (empty($arsip)) { ?>
                        data belum ada
                    <?php } elseif ($arsip->last == 9) { ?>
                        Anda sudah menyelesaikan KP.
                    <?php } else { ?>
                        <div id="rootwizard">
                            <form id="wizardForm" method="post" action="<?php echo base_url() ?>revisi/revisi/<?php echo $arsip->id_kerja ?>" role="form" enctype="multipart/form-data">
                                <div class="tab-content">
                                    <div class="tab-pane active fade in" id="tab1">
                                        <div class="row m-b-lg">
                                            <div class="col-md-6 center">
                                                <div class="row center">
                                                    <div class="form-group col-md-12">
                                                        <label for="exampleInputName">Judul KP/TA : </label>                                          
                                                        <input type="text" class="form-control" id="input-readonly" value="<?php echo $arsip->judul ?>" readonly>                                                        
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="exampleInputName">Jenis : </label>
                                                    <input type="text" class="form-control" id="input-readonly" value="<?php
                                                    echo $arsip->jenis;
                                                    ?>" readonly>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label for="exampleInputName">Dosen Pembimbing : </label>
                                                    <input type="text" class="form-control" id="input-readonly" value="<?php
                                                    foreach ($dosen as $number => $kerja1) {
                                                        echo $kerja1->nama;
                                                    }
                                                    ?>" readonly>
                                                </div>


                                                <div class="col-md-12">
                                                    <?php if (!empty($arsip->files)) { ?>
                                                        <a href="<?php echo base_url() ?>assets/files/<?php echo $arsip->files ?>"><i class="fa fa-download mg-r-sm">&nbsp;&nbsp;Download Dokumen</i></a>
                                                    <?php } ?>
                                                    <div class="panel panel-white">
                                                        <div class="panel-body">
                                                            <label >Upload Revisi : </label>
                                                            <input name="file" type="file"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="btn col-md-6">
                                                    <button type="submit" class="btn btn-success" name="submit" value="simpan">Submit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

