<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $title ?></title>
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta charset="UTF-8">
        <meta name="description" content="Finan Assignment Management System" />
        <meta name="keywords" content="simson uii, informatika, simson, skripsi" />
        <meta name="author" content="lax" />
        <!-- Styles -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
        <link href="<?php echo base_url() ?>assets/plugins/pace-master/themes/blue/pace-theme-flash.css" rel="stylesheet"/>
        <link href="<?php echo base_url() ?>assets/plugins/uniform/css/uniform.default.min.css" rel="stylesheet"/>
        <link href="<?php echo base_url() ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>assets/plugins/fontawesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>assets/plugins/line-icons/simple-line-icons.css" rel="stylesheet" type="text/css"/>	
        <link href="<?php echo base_url() ?>assets/plugins/offcanvasmenueffects/css/menu_cornerbox.css" rel="stylesheet" type="text/css"/>	
        <link href="<?php echo base_url() ?>assets/plugins/waves/waves.min.css" rel="stylesheet" type="text/css"/>	
        <link href="<?php echo base_url() ?>assets/plugins/switchery/switchery.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>assets/plugins/3d-bold-navigation/css/style.css" rel="stylesheet" type="text/css"/>	
        <link href="<?php echo base_url() ?>assets/css/modern.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>assets/css/themes/white.css" class="theme-color" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>assets/css/custom.css" rel="stylesheet" type="text/css"/>
        <script src="<?php echo base_url() ?>assets/plugins/3d-bold-navigation/js/modernizr.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/offcanvasmenueffects/js/snap.svg-min.js"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body class="page-login">
    <main class="page-content">
        <div class="page-inner">
            <div id="main-wrapper">
                <div class="row">
                    <div class="col-md-3 center">
                        <div class="login-box">
                            <a href="<?php echo base_url() ?>content" class="logo-name text-lg text-center">SIMKP</a>
                            <p class="text-center m-b-md">Sistem Manajemen Kerja Praktek.<br/>Silahkan Login !</p>
                            <?php echo $this->session->flashdata('pesan'); ?>
                            <form role="form" method="post" action="<?php echo base_url(); ?>login/verify">
                                <div class="form-group">
                                    <label><i class="fa fa-user m-r-sm"></i>Nomor Induk</label>
                                    <input type="text" name="user_nim" class="form-control" placeholder="Masukkan Nomor Induk Anda...">
                                </div>
                                <p class="help-block"><?php echo form_error('user_nim'); ?></p>
                                <div class="form-group">
                                    <label><i class="fa fa-key m-r-sm"></i>Password</label>
                                    <input type="password" name="user_password" class="form-control" placeholder="Masukkan Password Anda...">
                                </div>
                                <p class="help-block"><?php echo form_error('user_password'); ?></p>
                                <button type="submit" class="btn btn-success btn-block"><i class="fa fa-sign-in m-r-sm"></i>Login</button>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <a href="<?php echo base_url() ?>#" class="display-block m-t-md text-sm pull-left">Lupa Password ?</a>
                                    </div>
                                </div>
                            </form>
                            <div class="row m-t-md text-center">
                                <a href="<?php echo base_url() ?>content" class="m-r-md"><i class="fa fa-home m-r-xs"></i>Home</a><a href="<?php echo base_url() ?>kerja" class="m-r-md"><i class="fa fa-building m-r-xs"></i>Arsip KP/TA</a><a href="<?php echo base_url() ?>download"><i class="fa fa-download m-r-xs"></i>Download</a>
                            </div>
                            <div class="row">
                                <p class="text-center m-t-xs text-sm"><?php echo date("Y") ?> &copy; Teknik Informatika UII.<br/>Powered by LAX.</p>
                            </div>
                        </div>
                    </div>
                </div><!-- Row -->
            </div><!-- Main Wrapper -->
        </div><!-- Page Inner -->
    </main><!-- Page Content -->
    <!-- Javascripts -->
    <script src="<?php echo base_url() ?>assets/plugins/jquery/jquery-2.1.3.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/pace-master/pace.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/switchery/switchery.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/offcanvasmenueffects/js/classie.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/offcanvasmenueffects/js/main.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/waves/waves.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/3d-bold-navigation/js/main.js"></script>
    <script src="<?php echo base_url() ?>assets/js/modern.min.js"></script>

</body>
</html>