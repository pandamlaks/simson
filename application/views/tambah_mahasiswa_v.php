<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    <?php echo $this->session->flashdata('message'); ?>
                    <div id="rootwizard">                        
                        <form id="wizardForm" method="post" action="<?php echo base_url() ?>mahasiswa/submit">
                            <div class="tab-content">
                                <div class="tab-pane active fade in" id="tab1">
                                    <div class="row m-b-lg">
                                        <div class="col-md-6 center">
                                            <div class="row center">
                                                <div class="form-group col-md-7">
                                                    <label for="exampleInputName">Nama</label>
                                                    <input type="text" class="form-control" name="nama" id="exampleInputName" placeholder="Masukkan nama" required>
                                                </div>
                                                <div class="form-group  col-md-5">
                                                    <label for="exampleInputName2">Angkatan</label>
                                                    <input type="text" class="form-control col-md-6" name="angkatan" id="exampleInputName2" placeholder="Masukkan angkatan" required>
                                                </div>
                                                <div class="form-group  col-md-5">
                                                    <label for="exampleInputName2">Nomor Induk</label>
                                                    <input type="text" class="form-control col-md-6" name="nomor" id="exampleInputName2" placeholder="Masukkan nomor induk" required>
                                                </div>
                                                <div class="form-group col-md-12">
                                                        <label for="exampleInputJurusan">Jurusan</label>
                                                        <select class="form-control m-b-sm" name="jurusan" onchange="kons()">
                                                            <option value=" ">Pilih Jurusan</option>
                                                            <option value="Teknik-Kimia">Teknik Kimia</option>
                                                            <option value="Teknik-Industri">Teknik Industri</option>                                                            
                                                            <option value="Teknik-Informatika">Teknik Informatika</option>
                                                            <option value="Teknik-Elektro">Teknik Elektro</option>
                                                            <option value="Teknik-Mesin">Teknik Mesin</option>
                                                        </select>
                                                    </div>
                                                    <div id="konsentrasis"> </div>
                                                <div class="form-group col-md-5">
                                                        <label for="exampleInputJurusan">Telepon</label>
                                                        <input type="text" class="form-control" name="kontak" id="exampleInputEmail" placeholder="Masukkan telepon" required>
                                                    </div>
                                                    <div class="form-group col-md-7">
                                                        <label for="exampleInputEmail">Email</label>
                                                        <input type="email" class="form-control" name="email" id="exampleInputEmail" placeholder="Masukkan email">
                                                    </div> 
                                                <div class="form-group col-md-12">
                                                    <label for="exampleInputPassword1">Password</label>
                                                    <input type="password" class="form-control" name="password" id="exampleInputPassword1" placeholder="Masukkan password" required>
                                                </div>
                                                <div class="col-md-6">
                                                    <button type="submit" class="btn btn-success" name="submit" value="simpan">Confirm</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function kons() {
        var jurusan = $("select[name=jurusan] option:selected").val();
        if(jurusan != ' '){
            $("#konsentrasis").load("<?php echo base_url() ?>daftar/konsentrasi/" + jurusan);
        }else{
        $("#konsentrasis").html(" ");
        }
    }
    ;
</script>