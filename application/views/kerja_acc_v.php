
<div id="main-wrapper">

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">

                <div class="panel-body">
                    
                        <?php echo $this->session->flashdata('message'); ?>
                   
                    <div class="table-responsive">                        
                        <table id="table_id" class="display table" style="width: 100%; cellspacing: 0;">
                            <?php if ($sess['id_role'] == 3) { ?>
                                <thead class="text-center">
                                    <tr>
                                        <th>Judul</th>
                                        <th>Tempat</th>
                                        <th>Tanggal Mulai</th>
                                        <th>Tanggal Bimbingan</th>
                                        <th>Keterangan</th>
                                        <th>Request</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Judul</th>
                                        <th>Tempat</th>
                                        <th>Tanggal Mulai</th>
                                        <th>Tanggal Bimbingan</th>
                                        <th>Keterangan</th>
                                        <th>Request</th>
                                    </tr>
                                </tfoot>

                            <?php } elseif ($sess['id_role'] == 6) { ?>
                                <thead class="text-center">
                                    <tr>
                                        <th>Judul</th>
                                        <th>Tempat</th>
                                        <th>Alamat</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>

                            <?php } elseif ($sess['id_role'] == 2) { ?>
                                <thead>
                                    <tr>
                                        <!--<th>Jenis</th>-->
                                        <th>Judul</th>
                                        <th>Tempat</th>
                                        <th>Alamat</th>
                                        <!--<th>Status</th>-->
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <!--<th>Jenis</th>-->
                                        <th>Judul</th>
                                        <th>Tempat</th>
                                        <th>Alamat</th>                                    
                                        <!--<th>Status</th>-->
                                        <th>Aksi</th>
                                    </tr>
                                </tfoot>
                            <?php } elseif ($sess['id_role'] == 4) { ?>
                                <thead>
                                    <tr>
                                        <!--<th>Jenis</th>-->
                                        <th>Judul</th>
                                        <th>Tempat</th>
                                        <th>Alamat</th>
                                        <!--<th>Status</th>-->
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <!--<th>Jenis</th>-->
                                        <th>Judul</th>
                                        <th>Tempat</th>
                                        <th>Alamat</th>                                    
                                        <!--<th>Status</th>-->
                                        <th>Aksi</th>
                                    </tr>
                                </tfoot>
                            <?php } else { ?>
                                <thead>    
                                    <tr>
                                        <th>Judul KP</th>
                                        <th>Nama</th>
                                        <th>Tempat</th>
                                        <th>Tgl Daftar</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Judul KP</th>
                                        <th>Nama</th>
                                        <th>Tempat</th>
                                        <th>Tgl Daftar</th>
                                        <th>Aksi</th>
                                    </tr>
                                <?php } ?>
                            </tfoot>
                            <tbody>
                                <?php
                                foreach ($arsip as $row) {
                                    ?>
                                    <tr>
                                        <?php if ($sess['id_role'] == 3) { ?>
                                            <td> <?php echo $row->judul ?> <br>
                                                <?php echo $row->nomor ?> <?php echo $row->nama ?></td>
                                            <td> <?php echo $row->tempat ?> <br> 
                                                <?php echo $row->alamat ?> </td>
                                            <td> <?php echo date("d/m/Y H:i", strtotime($row->tgl_eksekusi)) ?> </td>
                                            <td> <?php echo date("d/m/Y H:i", strtotime($row->waktu)) ?> </td>
                                            <td> <?php echo $row->keterangan ?> </td>

                                        <?php } elseif ($sess['id_role'] == 2) { ?>
                                            <?php echo $row->jenis ?> </td>
                                            <td> <?php echo $row->judul ?> <br>
                                                <?php echo $row->nomor ?> <?php echo $row->nama ?></td>
                                            <td> <?php echo $row->tempat ?> </td>
                                            <td> <?php echo $row->alamat ?> </td>

                                                                <!--                                            <td> <?php
                                            if ($row->is_aktif == 0) {
                                                echo "Belum Aktif";
                                            } elseif ($row->is_aktif == 1) {
                                                echo "Aktif";
                                            } else {
                                                echo "Selesai";
                                            }
                                            ?>
                                                                                                            </td>-->

                                        <?php } elseif ($sess['id_role'] == 4) { ?>
                                            <?php echo $row->jenis ?> </td>
                                            <td> <?php echo $row->judul ?> <br>
                                                <?php echo $row->nomor ?> <?php echo $row->nama ?></td>
                                            <td> <?php echo $row->tempat ?> </td>
                                            <td> <?php echo $row->alamat ?> </td>

                                        <?php } elseif ($sess['id_role'] == 6) { ?>
                                            <td> <?php echo $row->judul ?> </td>
                                            <td> <?php echo $row->tempat ?> </td>
                                            <td> <?php echo $row->alamat ?> </td>
                                        <?php } else { ?>
                                            <td> <?php echo $row->judul ?> </td>
                                            <td> <?php echo $row->nama ?> <br>
                                                <?php echo $row->nomor ?> </td>
                                            <td> <?php echo $row->tempat ?> <br>
                                                <?php echo $row->alamat ?> </td>
                                            <td> <?php echo $row->tgl_daftar ?> </td>
                                        <?php } ?>
                                        <td>
                                            <?php if ($sess['id_role'] == 3) { ?>
                                                <a href="<?php echo base_url() ?>bimbingan/status/<?php echo $row->id_bimbingan ?>/<?php echo $row->id_kerja ?>/1"><b>Accept</b></a><br>
                                                <a href="<?php echo base_url() ?>bimbingan/status/<?php echo $row->id_bimbingan ?>/<?php echo $row->id_kerja ?>/2"><b>Reject</b></a>
                                            <?php } elseif ($sess['id_role'] == 2) { ?>
                                                <a href="<?php echo base_url() ?>kerja/status/<?php echo $row->id_kerja ?>/2"><b>Terima Judul</b></a><br>
                                                <a href="<?php echo base_url() ?>kerja/form_reject/<?php echo $row->id_kerja ?>"><b>Tolak Judul</b></a>
                                            <?php } elseif ($sess['id_role'] == 4) { ?>
                                                <a href="<?php echo base_url() ?>kerja/status/<?php echo $row->id_kerja ?>/2"><b>Terima Judul</b></a><br>
                                                <a href="<?php echo base_url() ?>kerja/form_reject/<?php echo $row->id_kerja ?>"><b>Tolak Judul</b></a>
                                            <?php } elseif ($sess['id_role'] == 5) { ?>
                                                <a href="<?php echo base_url() ?>kerja/status2/<?php echo $row->id_kerja ?>/3"><b>Cetak</b></a>
                                            <?php } elseif ($sess['id_role'] == 6) { ?>
                                                <a href="<?php echo base_url() ?>kerja/status3/<?php echo $row->id_kerja ?>/4"><b>Diterima Perusahaan</b></a><br>
                                                <a href="<?php echo base_url() ?>kerja/status3/<?php echo $row->id_kerja ?>/10"><b>Ditolak Perusahaan</b></a>

                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url() ?>assets/plugins/jquery/jquery-2.1.3.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/js/jquery.datatables.min.js"></script>
<script>
    $('#table_id').DataTable();
</script>