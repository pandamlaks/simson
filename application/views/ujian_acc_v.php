
<div id="main-wrapper">

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-heading clearfix">

                </div>
                <div class="panel-body">
                    <?php echo $this->session->flashdata('message'); ?>
                    <div class="table-responsive">

                        <table id="table_id" class="display table" style="width: 100%; cellspacing: 0;">

                            <thead class="text-center">
                                <tr>
                                    <th>Judul</th>
                                    <th>Tempat</th>
                                    <th>Set Tanggal</th>
                                    <th>Aksi</th>

                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Judul</th>
                                    <th>Tempat</th>
                                    <th>Set Tanggal</th>
                                    <th>Aksi</th>
                                </tr>
                            </tfoot>
                            <?php
                            $no = 1;
                            foreach ($arsip as $row) {
                                ?>
                                <tr>

                                    <td> <?php echo $row->judul ?> <br>
                                        <?php echo $row->nomor ?> <?php echo $row->nama ?></td>
                                    <td> <?php echo $row->tempat ?> <br> 
                                        <?php echo $row->alamat ?> </td>
                                    <td><input type="text" class="form-control date-format date<?php echo $no ?>" name="tanggal" id="exampleInputName"></td>


                                    <td class="text-center">

                                        <a role="button" onclick="statusUj(<?php echo $row->id_kerja ?>, 1,<?php echo $no ?>)"><b>Accept</b></a><br>
                                        <a role="button" onclick="statusUj(<?php echo $row->id_kerja ?>, 2,<?php echo $no ?>)"><b>Reject</b></a><br>

                                    </td>
                                </tr>
                                <?php $no++;
                            }
                            ?>
                            </tbody>
                        </table>
                        <script>
                            function statusUj(id, st, pos) {
                                if ($(".date" + pos).val() == '') {
                                    alert("Tanggal tidak boleh kosong");
                                    return false;
                                } else {
                                    window.location.href = "<?php echo base_url() ?>ujian/status/" + id + "/" + st + "/" + $(".date" + pos).val();
                                }
                            }
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url() ?>assets/plugins/jquery/jquery-2.1.3.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/js/jquery.datatables.min.js"></script>
<script>
                            $('#table_id').DataTable();
</script>