<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    <?php echo $this->session->flashdata('message'); ?>
                    <?php if (!empty($arsip)) { ?>
                        <?php if ($arsip->last != 3) { ?>
                            Anda sudah menyelsaikan permohonan
                        <?php } else { ?>
                            <div id="rootwizard">
                                <form id="wizardForm" method="post">
                                    <div class="tab-content">
                                        <div class="tab-pane active fade in" id="tab1">
                                            <div class="row m-b-lg">
                                                <div class="col-md-6 center">
                                                    <div class="row center">
                                                        <div class="form-group col-md-12">
                                                            <label for="exampleInputName">Judul KP/TA : </label>                                          
                                                            <input type="text" class="form-control" id="input-readonly" value="<?php echo $arsip->judul ?>" readonly>                                                        
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label for="exampleInputName">Tempat : </label>
                                                        <input type="text" class="form-control" id="input-readonly" value="<?php echo $arsip->tempat ?>" readonly>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label for="exampleInputName">Alamat : </label>
                                                        <input type="text" class="form-control" id="input-readonly" value="<?php echo $arsip->alamat ?>" readonly>
                                                    </div>                                                
                                                    <div class="col-md-6">
                                                        <a href="<?php echo base_url() ?>kerja/status3/<?php echo $arsip->id_kerja ?>/4" class="btn-btn-success"><b>Diterima Perusahaan</b></a>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <a href="<?php echo base_url() ?>kerja/status3/<?php echo $arsip->id_kerja ?>/10" class="btn-btn-success"><b>Ditolak Perusahaan</b></a>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        Data belum ada / sudah melapor
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>


