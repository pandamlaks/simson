
<div id="main-wrapper">

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title"</h4>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">

                        <table id="table_id" class="display table" style="width: 100%; cellspacing: 0;">
                            <thead>
                                <tr>

                                    <th>Judul</th>
                                    <th>Tempat</th>                                    
                                    <th>Dosen Pembimbing</th>
                                    <th>Aksi</th>

                                </tr>
                            </thead>
                            <tfoot>
                                <tr>

                                    <th>Judul</th>
                                    <th>Tempat</th>
                                    <th>Dosen Pembimbing</th>
                                    <th>Aksi</th>

                                </tr>
                            </tfoot>
                            <tbody>
                                <?php
                                foreach ($arsip as $row) {
                                    ?>

                                    <tr>
                                <form action="<?php echo base_url() ?>kerja/saveset_dosen/<?php echo $row->id_kerja ?>" method="post">
                                    <td> <?php echo $row->judul ?> <br>
                                        <?php echo $row->nim ?> <?php echo $row->mahasiswa ?> </td>
                                    <td> <?php echo $row->tempat ?> <br>
                                        <?php echo $row->alamat ?> </td>
                                    <td><select class="form-control m-b-sm" name="dosen">
                                            <?php
                                            foreach ($dosen as $number => $row) {
                                                echo '<option value="' . $row->id_karyawan . '">' . $row->nama . ', ' . $row->gelar . '</option>';
                                            }
                                            ?></select>

                                    </td> 
                                    <td>
                                        <button type="submit" value="simpan">Simpan</button>
                                    </td>
                                </form>
                                </tr>

                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() ?>assets/plugins/jquery/jquery-2.1.3.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/js/jquery.datatables.min.js"></script>
<script>
    $('#table_id').DataTable();
</script>