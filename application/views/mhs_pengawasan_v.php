
<div id="main-wrapper">

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title">Pengawasan</h4>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <div id="example_length" class="dataTables_length">

                            <form action="<?php echo base_url()?>mahasiswa/pengawasan" method="post">
                                <label>Search <select class="" aria-controls="example" name="status">
                                        <!--<option value="1">Semua Mahasiswa Terlambat</option>-->
                                        <option value="2" <?php echo $status == 2?"selected":null?>>Mahasiswa Terlambat Mulai</option>
                                        <option value="3" <?php echo $status == 3?"selected":null?>>Mahasiswa Terlambat Bimbingan</option>
                                        <option value="4" <?php echo $status == 4?"selected":null?>>Mahasiswa Terlambat Revisi</option>
                                        <option value="5" <?php echo $status == 5?"selected":null?>>Mahasiswa Terlambat Selesai</option>
                                    </select> </label>
                                <button type="submit">Submit</button>
                            </form>
                        </div>
                        <table id="table_id" class="display table" style="width: 100%; cellspacing: 0;">
                            <thead>
                                <tr>
                                    <th>NIM</th>
                                    <th>Nama</th>
                                    <th>Kontak</th>                                    
                                    <th>Pembimbing</th>
                                    <th>Tgl. Daftar</th>
                                    <th>Tgl. Mulai</th>
                                    <th>Tgl. Bimbingan</th>
                                    <th>Aksi</th>

                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>NIM</th>
                                    <th>Nama</th>
                                    <th>Kontak</th>                                    
                                    <th>Pembimbing</th>
                                    <th>Tgl. Daftar</th>
                                    <th>Tgl. Mulai</th>
                                    <th>Tgl. Bimbingan</th>
                                    <th>Aksi</th>

                                </tr>
                            </tfoot>
                            <tbody>
                                <?php
                                foreach ($pengguna as $row) {
//                                    $datetime1 = new DateTime($row->daftar);
//                                    $datetime2 = new DateTime(date("Y-m-d H:i:s"));
//                                    $difference = $datetime1->diff($datetime2);
//                                    if ($difference->days > 180) {
                                        ?>
                                        <tr>
                                            <td> <?php echo $row->nim ?></td>
                                            <td> <?php echo $row->mhs ?> </td>
                                            <td> <?php echo $row->kontak ?> </td>
                                            <td> <?php echo $row->dosen ?> </td>
                                            <td> <?php echo $row->daftar ?> </td>
                                            <td> <?php echo $row->mulai ?> </td>
                                            <td> <?php echo $row->bimbingan ?> </td>
                                            <td></td>
                                        <?php // }
                                    } ?>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#table_id').DataTable();
    });
</script>