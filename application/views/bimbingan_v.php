<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<div class="page-title">
    <h3>Request Bimbingan</h3>                    
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    <?php echo $this->session->flashdata('message'); ?>
                    <?php if (empty($arsip)) { ?>
                        data belum ada
                    <?php } elseif ($arsip->last == 9) { ?>
                        Anda sudah menyelesaikan KP.
                    <?php } else { ?>
                        <div id="rootwizard">
                            <form id="wizardForm" method="post" action="<?php echo base_url() ?>bimbingan/bimbingan">
                                <div class="tab-content">
                                    <div class="tab-pane active fade in" id="tab1">
                                        <div class="row m-b-lg">
                                            <div class="col-md-6 center">
                                                <div class="row center">
                                                    <div class="form-group col-md-12">
                                                        <label for="exampleInputName">Judul KP/TA : </label>                                          
                                                        <input type="text" class="form-control" id="input-readonly" value="<?php echo $arsip->judul ?>" readonly>                                                        
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label for="exampleInputName">Dosen Pembimbing : </label>
                                                    <input type="text" class="form-control" id="input-readonly" value="<?php
                                                    foreach ($dosen as $number => $kerja1) {
                                                        echo $kerja1->nama;
                                                    }
                                                    ?>" readonly>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="exampleInputName2">Tanggal</label>  
                                                    <input type="text" name="tanggal" class="form-control datetime-format" id="exampleInputName" required <?php echo!empty($cek) ? $cek->is_disetujui == 0 ? "disabled" : null : null ?>>
                                                </div>
<!--                                                <div class="col-md-6" style="margin-top: 22px">
                                                    <i class="fa fa-info text-info"></i>&nbsp;&nbsp;<small><i>Format penulisan tanggal: <b>dd-mm-yyyy</b>(spasi)<b>hh:mm</b></i></small>
                                                </div>-->
                                                <div class="form-group col-md-12">
                                                    <label for="exampleInputEmail">Materi Bimbingan</label>
                                                    <input type="text" class="form-control" name="materi" id="exampleInputName" <?php echo!empty($cek) ? $cek->is_disetujui == 0 ? "disabled" : null : null ?>>
                                                </div>
                                                <div class="col-md-6">
                                                    <button type="submit" class="btn btn-success" name="submit" value="simpan" <?php echo!empty($cek) ? $cek->is_disetujui == 0 ? "disabled" : null : null ?>>Submit Request</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if (!empty($cek)) { ?>
    <?php if ($cek->is_disetujui == 0) { ?>
        <script>
            $(document).ready(function () {
                $("input").attr("disabed", "disabed");
                $("button[type=submit]").attr("disabled", "disabled");
            });
        </script>
    <?php } ?>
<?php } ?>



