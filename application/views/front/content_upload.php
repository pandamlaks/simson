<script src="<?php echo base_url() ?>tinymce/tinymce.min.js" type="text/javascript"></script>
<script>
    tinymce.init({
        selector: "textarea#elm1",
        theme: "modern",
        height: 350,
        subfolder: "content",
        relative_urls: false,
        plugins: [
            "advlist autolink link lists charmap hr anchor pagebreak",
            "searchreplace wordcount insertdatetime nonbreaking",
            "table contextmenu directionality emoticons paste textcolor"
        ],
        content_css: "css/content.css",
        image_advtab: false,
        toolbar: "undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link unlink | forecolor backcolor",
//        style_formats: [
//            {title: 'Bold text', inline: 'b'},
//            {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
//            {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
//            {title: 'Example 1', inline: 'span', classes: 'example1'},
//            {title: 'Example 2', inline: 'span', classes: 'example2'},
//            {title: 'Table styles'},
//            {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
//        ]
    });
</script>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    <div id="rootwizard">
                        <form id="wizardForm" enctype="multipart/form-data" method="post" action="<?php echo base_url() ?>content/submit_content">
                            <div class="tab-content">
                                <div class="tab-pane active fade in" id="tab1">
                                    <div class="row m-b-lg">
                                        <div class="col-md-6 center">
                                            <div class="row center">
                                                <div class="form-group col-md-12">
                                                    <label for="exampleInputName">Judul</label>
                                                    <input type="text" class="form-control" name="judul"  id="exampleInputName" placeholder="Masukkan judul">
                                                </div>                                              
                                                <div class="form-group col-md-12">
                                                    <label for="exampleInputEmail">Gambar</label>
                                                    <input type="file" name="gambar" />
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label for="exampleInputPassword1">Isi</label>
                                                    <textarea class="form-control" name="isi" id='elm1' placeholder="Tulis isi berita" ></textarea>
                                                </div>
                                                <!--                                                <div class="form-group col-md-12">
                                                                                                    <label for="exampleInputEmail">Lampiran</label>
                                                                                                    <input type="text" class="form-control" name="lampiran"  id="exampleInputName" placeholder="Upload Lampiran" >
                                                                                                </div>-->

                                                <div class="col-md-6">
                                                    <button type="submit" class="btn btn-success" name="submit" value="simpan">Post</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>