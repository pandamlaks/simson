<div id="main-wrapper">
    <div class="row">
        <div class="col-sm-8">
            <?php if (count($berita) > 0) { ?>
                <?php foreach ($berita as $number => $row) { ?>
                    <div class="row">
                        <div class="panel panel-body">
                            <?php if (!empty($row->gambar)) { ?>
                                <div class="col-sm-4">
                                    <div class="col-xs-12" style="overflow: hidden; height: 250px">
                                        <img style="height: 100%" src="<?php echo base_url("assets/files/$row->gambar") ?>"/>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="<?php echo!empty($row->gambar) ? "col-sm-8" : "col-sm-12" ?>">
                                <h2><?php echo ucwords($row->judul) ?></h2><hr class="divide-bottom" style="margin-bottom: 5px"/>
                                <small><b><?php echo ucfirst($row->nama) ?></b> | Published on <b><?php echo date("d-m-Y", strtotime($row->tanggal)) ?></b></small>
                                <hr class="divide-bottom" style="margin-top: 7px"/>
                                <p class="text-justify"><?php echo substr($row->isi, 0, 200) ?></p>
                                <a href="<?php echo base_url("content/index/$row->id_content") ?>" class="btn btn-sm btn-info">Read more <i class="fa fa-chevron-right m-l-sm"></i></a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            <?php } else { ?>
                <h3><b><i class="fa fa-warning mg-r-sm"></i></b><i>Belum ada artikel di posting...</i></h3>
            <?php } ?>
        </div>
        <div class="col-sm-4">
            <?php if (count($document) > 0) { ?>
                <h3><i class="fa fa-clipboard m-r-sm"></i>Dokumen Terbaru</h3><hr class="divide-bottom"/>
                <?php foreach ($document as $no => $rox) { ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-xs-10">
                                    <h4><i class="fa fa-chevron-right m-r-xs"></i><?php echo $rox->judul_doc ?></h4>
                                    <p style="padding-left: 20px"><i>Published on <b><?php echo date("d-m-Y", strtotime($rox->tanggal_doc)) ?></i></b></p>
                                </div>
                                <div class="col-xs-2 p-v-md">
                                    <a href="<?php echo base_url($rox->lampiran) ?>">
                                        <i class="fa fa-download" style="font-size: 25px"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>

            <h3><i class="fa fa-th-large m-r-sm"></i>BANNER</h3><hr class="divide-bottom"/>
            <div class="panel panel-body">
                <a href="https://klasiber.uii.ac.id/"><img class="col-xs-12" src="http://informatics.uii.ac.id/new/images/klasiber-small.png"/></a>
            </div>
        </div>
    </div>
</div>