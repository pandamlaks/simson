
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title">Dosen Pembimbing</h4>
                </div>
                <div class="panel-body">
                    <?php echo $this->session->flashdata('message'); ?>
                    <div class="table-responsive">
                        
                        <table id="table_id" class="display table" style="width: 100%; cellspacing: 0;">
                            <thead>
                                <tr>
                                    <th>Nomor</th>
                                    <th>Nama</th>
                                    <th>Jurusan</th>
                                    <th>Email</th>
                                    <th>KP</th>
                                    <th>TA</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Nomor</th>
                                    <th>Nama</th>
                                    <th>Jurusan</th>
                                    <th>Email</th>
                                    <th>KP</th>
                                    <th>TA</th>
                                    <th></th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php foreach ($dosen as $row) { ?>
                                <form action="<?php echo base_url() ?>karyawan/set_bimbing/<?php echo $row->id_karyawan ?>" method="post">
                                    <tr>
                                        <td> <?php echo $row->nomor ?> </td>
                                        <td> <?php echo $row->nama ?>, <?php echo $row->gelar ?> </td>
                                        <td> <?php echo $row->jurusan ?> </td>
                                        <td> <?php echo $row->email ?> </td>
                                        <td> 
                                            <select class="form-control m-b-sm" name="bimbingKP">
                                                <option value="0" <?php echo $row->isPembimbingKP == 0 ? "selected" : null ?>>Tidak Bisa</option>
                                                <option value="1" <?php echo $row->isPembimbingKP == 1 ? "selected" : null ?>>Bisa</option>
                                            </select> 
                                        </td>
                                        <td> 
                                            <select class="form-control m-b-sm" name="bimbingTA">
                                                <option value="0" <?php echo $row->isPembimbingTA == 0 ? "selected" : null ?>>Tidak Bisa</option>
                                                <option value="1" <?php echo $row->isPembimbingTA == 1 ? "selected" : null ?>>Bisa</option> 
                                            </select>
                                        </td>
                                        <td>
                                            <button type="submit" value="simpan">Set</button>
                                        </td>
                                    </tr>
                                </form>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() ?>assets/plugins/jquery/jquery-2.1.3.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/js/jquery.datatables.min.js"></script>
<script>
        $('#table_id').DataTable();
</script>