<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title">Proses KP</h4>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">                        
                        <table id="example" class="display table" style="width: 100%; cellspacing: 0;">
                            <thead>
                                <tr>
                                    <th>Aktivitas</th>
                                    <th>Pengajuan</th>
                                    <th>Persetujuan</th>                                    
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Aktivitas</th>
                                    <th>Pengajuan</th>
                                    <th>Persetujuan</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php foreach ($proses as $row) {
                                    ?>                                
                                    <tr>
                                        <td>Pendaftaran</td>
                                        <td><a href="<?php echo base_url() ?>daftar" class="waves-effect waves-button">Form Pendaftaran KP</a></td>
<!--                                        <td><?php
                                            if ($row->daftar == true) {
                                                echo $row->daftar;
                                            } else {
                                                echo "";
                                            }
                                            ?>
                                        </td>-->
                                        <td> - </td>
                                    </tr>
                                    <tr>
                                        <td>Permohonan ke Perusahaan</td>
                                        <td><a href="<?php echo base_url() ?>kerja/acc3" class="waves-effect waves-button">Form Permohonan</a></td>
                                        <td> - </td>
                                    </tr>
                                    <tr>
                                        <td>Pelaksanaan KP</td>
                                        <td> - </td>
                                        <td> - </td>
                                    </tr>
                                    <tr>
                                        <td>Bimbingan</td>
                                        <td><a href="<?php echo base_url() ?>bimbingan" class="waves-effect waves-button">Form Bimbingan</a></td>
                                        <td> - </td>
                                    </tr>
                                    <tr>
                                        <td>Ujian</td>
                                        <td><a href="<?php echo base_url() ?>ujian" class="waves-effect waves-button">Form Ujian</a></td>
                                        <td> - </td>
                                    </tr>
                                    <tr>
                                        <td>Penyerahan Laporan Akhir</td>
                                        <td><a href="<?php echo base_url() ?>revisi" class="waves-effect waves-button">Closing</td>
                                    <td> - </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>