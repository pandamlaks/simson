<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">

                    <?php echo $this->session->flashdata('message'); ?>

                    <div id="rootwizard">
                        <form id="wizardForm" method="post" action="<?php echo base_url() ?>daftar/submit">
                            <div class="tab-content">
                                <div class="tab-pane active fade in" id="tab1">
                                    <div class="row m-b-lg">
                                        <div class="col-md-6 center">
                                            <div class="row center">
                                                <?php if (count($check) > 0) { ?>                                                
                                                    <div class="col-md-12" style="padding-top: 7px">
                                                        <i class="fa fa-warning text-warning"></i>&nbsp;&nbsp;&nbsp;&nbsp;<small><i>Silahkan menunggu persetujuan judul dari jurusan. Terima kasih.</i></small>
                                                    </div>
                                                <?php } ?>

                                                <div class="form-group col-md-12">
                                                    <label>Judul</label>
                                                    <input type="text" class="form-control" name="judul"  id="exampleInputName" placeholder="Masukkan Judul" required>
                                                </div>
                                                <!--                                                <div class="row">
                                                                                                    <div class="col-xs-12">
                                                                                                        <div class="col-xs-4">
                                                                                                    <label>Jenis</label>
                                                                                                    <input type="text" class="form-control" name="jenis" id="input-disabled" value="KP" disabled>
                                                                                                    <select name="jenis" class="form-control m-b-sm">
                                                                                                        <option value="KP">KP</option>
                                                                                                        <option value="TA">TA</option>
                                                                                                    </select>
                                                                                                </div>
                                                                                                <div class="col-xs-8">
                                                                                                    <label>Konsentrasi</label>
                                                                                                    <select class="form-control m-b-sm" name="konsentrasi">
                                                <?php
                                                foreach ($konsentrasi as $number => $row) {
                                                    echo '<option value="' . $row->id_konsentrasi . '">' . $row->konsentrasi . '</option>';
                                                }
                                                ?></select>
                                                                                                </div>
                                                                                                    </div>
                                                                                                </div>-->
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div class="col-xs-12">
                                                            <label>Tempat</label>
                                                            <input type="text" class="form-control" name="tempat"  id="exampleInputName" placeholder="Masukkan tempat KP/TA" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div class="col-xs-12" style="margin-top: 10px">
                                                            <label>Alamat</label>
                                                            <input type="text" class="form-control" name="alamat"  id="exampleInputPassword1" placeholder="Masukkan alamat KP/TA" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <button type="submit" class="btn btn-success" style="margin-top: 10px" name="submit" value="simpan">Confirm</button>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php if (count($check) > 0) { ?>
    <script>
        $("input").attr("disabled", "disabled");
        $("button").attr("disabled", "disabled");
        $("select").attr("disabled", "disabled");
    </script>
<?php } ?>