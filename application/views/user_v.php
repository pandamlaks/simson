<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    <?php echo $this->session->flashdata('message'); ?>
                    <div id="rootwizard">
                        <form id="wizardForm" method="post" action="<?php echo base_url() ?>user/edit">
                        <div class="tab-content">
                            <div class="tab-pane active fade in" id="tab1">
                                <div class="row m-b-lg">
                                    <div class="col-md-6 center">

                                        <div class="row center">
                                            <div class="form-group col-md-7">
                                                <label for="exampleInputName">Nama</label>
                                                <input type="text" class="form-control" name="nama" id="input-readonly" value="<?php echo $arsip->nama ?>">
                                            </div>
                                            <div class="form-group col-md-7">
                                                <label for="exampleInputName2">Nomor Induk</label>
                                                <input type="text" class="form-control col-md-6" name="nomor" id="input-readonly" value="<?php echo $arsip->nomor ?>" >
                                            </div>
                                            <div class="form-group col-md-7">
                                                <label for="exampleInputJurusan">Jurusan</label>
                                                <input type="text" class="form-control" name="jurusan" id="input-readonly" value="<?php echo $arsip->jurusan ?>" readonly="">
                                            </div>
                                            <div class="form-group col-md-7">
                                                <label for="exampleInputKonsentrasi">Konsentrasi</label>
                                                <input type="text" class="form-control" name="konsentrasi" id="input-readonly" value="<?php echo $arsip->konsentrasi ?>" readonly="">
                                            </div>                                            
                                            <div class="form-group col-md-7">
                                                <label for="exampleInputEmail">Email</label>
                                                <input type="email" class="form-control" name="email" id="input-readonly" value="<?php echo $arsip->email ?>" >
                                            </div>
                                            <div class="form-group col-md-7">
                                                <label for="exampleInputJurusan">Kontak</label>
                                                <input type="text" class="form-control" name="kontak" id="input-readonly" value="<?php echo $arsip->kontak ?>" >
                                            </div>      
                                            <div class="col-md-6">
                                                    <button type="submit" class="btn btn-success" name="submit" value="simpan">Edit</button>
                                                </div>
                                        </div>
                                    </div>                                        
                                </div>
                            </div>
                        </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
