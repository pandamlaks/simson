<?php

class MY_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function authentication_root() {
        if ($this->session->userdata(md5('simson'))) {
            $session_data = $this->session->userdata(md5('simson'));
            $user = $this->db->query("select * from tm_user u join tr_role r on r.id_role = u.id_role where u.id_user = '$session_data[user_id]'")->row();
            $karyawan = $this->db->query("select * from tm_karyawan k join tm_user u on u.id_user = k.id_user where u.id_user = $user->id_user")->row();
            if (!empty($karyawan)) {
                $data['karyawan'] = $karyawan->id_karyawan;
                $data['jurusan'] = $karyawan->jurusan;
            }
            $mahasiswa = $this->db->query("select * from tm_mahasiswa where id_user = $user->id_user")->row();
            if (!empty($mahasiswa)) {
                $data['mahasiswa'] = $mahasiswa->id_mahasiswa;
            }
            $data['id'] = md5($session_data['user_id']);
            $data['id_role'] = $user->id_role;
            $data['users'] = $session_data['user_id'];
            $data['level_role'] = $user->level_role;
            $data['nomor'] = $user->nomor;
            $data['nama'] = $user->nama;
            $data['email'] = $user->email;
            $data['kontak'] = $user->kontak;
            $data['login'] = md5($session_data['login']);
            return $data;
        }
    }

    public function user_session($module, $view, $data = FALSE) {
        $this->load->view($module . '/' . $view, $data);
    }

}
