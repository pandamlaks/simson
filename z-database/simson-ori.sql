-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 09, 2015 at 02:54 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `simson`
--

-- --------------------------------------------------------

--
-- Table structure for table `td_karyawan`
--

CREATE TABLE IF NOT EXISTS `td_karyawan` (
  `id_karyawan` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `gelar` varchar(16) DEFAULT NULL,
  `isPembimbingKP` tinyint(1) unsigned DEFAULT NULL,
  `isPembimbingTA` tinyint(1) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `td_kinerja`
--

CREATE TABLE IF NOT EXISTS `td_kinerja` (
  `id_kerja` int(11) NOT NULL,
  `id_karyawan` int(11) NOT NULL,
  `nilai` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tm_kerja`
--

CREATE TABLE IF NOT EXISTS `tm_kerja` (
  `id_kerja` int(11) NOT NULL,
  `id_karyawan` int(11) NOT NULL,
  `id_mahasiswa` int(11) NOT NULL,
  `id_konsentrasi` smallint(6) unsigned NOT NULL,
  `jenis` varchar(16) DEFAULT NULL,
  `judul` varchar(64) DEFAULT NULL,
  `tempat` varchar(32) DEFAULT NULL,
  `alamat` varchar(32) DEFAULT NULL,
  `is_aktif` tinyint(1) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tm_mahasiswa`
--

CREATE TABLE IF NOT EXISTS `tm_mahasiswa` (
  `id_mahasiswa` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `angkatan` year(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tm_user`
--

CREATE TABLE IF NOT EXISTS `tm_user` (
  `id_user` int(11) NOT NULL,
  `id_konsentrasi` smallint(6) unsigned NOT NULL,
  `id_role` smallint(6) unsigned NOT NULL,
  `nomor` varchar(9) DEFAULT NULL,
  `passwd` varchar(32) DEFAULT NULL,
  `nama` varchar(32) DEFAULT NULL,
  `jurusan` varchar(16) DEFAULT NULL,
  `email` varchar(32) DEFAULT NULL,
  `kontak` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tr_konsentrasi`
--

CREATE TABLE IF NOT EXISTS `tr_konsentrasi` (
  `id_konsentrasi` smallint(6) unsigned NOT NULL,
  `jurusan` varchar(16) DEFAULT NULL,
  `konsentrasi` varchar(48) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tr_role`
--

CREATE TABLE IF NOT EXISTS `tr_role` (
  `id_role` smallint(6) unsigned NOT NULL,
  `nama` varchar(16) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tr_status`
--

CREATE TABLE IF NOT EXISTS `tr_status` (
  `id_status` smallint(6) unsigned NOT NULL,
  `jenis` varchar(16) DEFAULT NULL,
  `nama` varchar(32) DEFAULT NULL,
  `status_next` smallint(6) unsigned DEFAULT NULL,
  `is_wajib` tinyint(1) unsigned DEFAULT NULL,
  `is_aktif` tinyint(1) unsigned DEFAULT NULL,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tt_bimbingan`
--

CREATE TABLE IF NOT EXISTS `tt_bimbingan` (
  `id_bimbingan` int(11) NOT NULL,
  `id_kerja` int(11) NOT NULL,
  `waktu` timestamp NULL DEFAULT NULL,
  `is_disetujui` tinyint(1) unsigned DEFAULT NULL,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tt_kerja_status`
--

CREATE TABLE IF NOT EXISTS `tt_kerja_status` (
  `id_kerja` int(11) NOT NULL,
  `id_karyawan` int(11) NOT NULL,
  `id_status` smallint(6) unsigned NOT NULL,
  `tgl_eksekusi` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tt_ujian`
--

CREATE TABLE IF NOT EXISTS `tt_ujian` (
  `id_ujian` int(11) NOT NULL,
  `id_kerja` int(11) NOT NULL,
  `id_karyawan` int(11) NOT NULL,
  `jenis` varchar(16) DEFAULT NULL,
  `waktu` timestamp NULL DEFAULT NULL,
  `is_disetujui` tinyint(1) unsigned DEFAULT NULL,
  `id_user_setuju` varchar(9) DEFAULT NULL,
  `wkt_dijadwalkan` timestamp NULL DEFAULT NULL,
  `files` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `td_karyawan`
--
ALTER TABLE `td_karyawan`
  ADD PRIMARY KEY (`id_karyawan`), ADD KEY `td_karyawan_FKIndex1` (`id_user`);

--
-- Indexes for table `td_kinerja`
--
ALTER TABLE `td_kinerja`
  ADD PRIMARY KEY (`id_kerja`), ADD KEY `tm_user_has_tm_kerja_FKIndex2` (`id_kerja`), ADD KEY `td_kinerja_FKIndex2` (`id_karyawan`);

--
-- Indexes for table `tm_kerja`
--
ALTER TABLE `tm_kerja`
  ADD PRIMARY KEY (`id_kerja`), ADD KEY `tm_kerja_FKIndex1` (`id_konsentrasi`), ADD KEY `tm_kerja_FKIndex2` (`id_mahasiswa`), ADD KEY `tm_kerja_FKIndex3` (`id_karyawan`);

--
-- Indexes for table `tm_mahasiswa`
--
ALTER TABLE `tm_mahasiswa`
  ADD PRIMARY KEY (`id_mahasiswa`), ADD KEY `tm_mahasiswa_FKIndex1` (`id_user`);

--
-- Indexes for table `tm_user`
--
ALTER TABLE `tm_user`
  ADD PRIMARY KEY (`id_user`), ADD KEY `tm_user_FKIndex1` (`id_role`), ADD KEY `tm_user_FKIndex2` (`id_konsentrasi`);

--
-- Indexes for table `tr_konsentrasi`
--
ALTER TABLE `tr_konsentrasi`
  ADD PRIMARY KEY (`id_konsentrasi`);

--
-- Indexes for table `tr_role`
--
ALTER TABLE `tr_role`
  ADD PRIMARY KEY (`id_role`);

--
-- Indexes for table `tr_status`
--
ALTER TABLE `tr_status`
  ADD PRIMARY KEY (`id_status`);

--
-- Indexes for table `tt_bimbingan`
--
ALTER TABLE `tt_bimbingan`
  ADD PRIMARY KEY (`id_bimbingan`), ADD KEY `tt_bimbingan_FKIndex1` (`id_kerja`);

--
-- Indexes for table `tt_kerja_status`
--
ALTER TABLE `tt_kerja_status`
  ADD PRIMARY KEY (`id_kerja`), ADD KEY `tm_user_has_tm_kerja_FKIndex2` (`id_kerja`), ADD KEY `tm_user_has_tm_kerja_FKIndex3` (`id_status`), ADD KEY `tt_kerja_status_FKIndex3` (`id_karyawan`);

--
-- Indexes for table `tt_ujian`
--
ALTER TABLE `tt_ujian`
  ADD PRIMARY KEY (`id_ujian`), ADD KEY `tt_ujian_FKIndex1` (`id_karyawan`), ADD KEY `tt_ujian_FKIndex2` (`id_kerja`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `td_karyawan`
--
ALTER TABLE `td_karyawan`
  MODIFY `id_karyawan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tm_kerja`
--
ALTER TABLE `tm_kerja`
  MODIFY `id_kerja` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tm_mahasiswa`
--
ALTER TABLE `tm_mahasiswa`
  MODIFY `id_mahasiswa` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tm_user`
--
ALTER TABLE `tm_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tr_konsentrasi`
--
ALTER TABLE `tr_konsentrasi`
  MODIFY `id_konsentrasi` smallint(6) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tr_role`
--
ALTER TABLE `tr_role`
  MODIFY `id_role` smallint(6) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tr_status`
--
ALTER TABLE `tr_status`
  MODIFY `id_status` smallint(6) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tt_bimbingan`
--
ALTER TABLE `tt_bimbingan`
  MODIFY `id_bimbingan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tt_ujian`
--
ALTER TABLE `tt_ujian`
  MODIFY `id_ujian` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
